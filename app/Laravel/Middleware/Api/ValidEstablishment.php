<?php

namespace App\Laravel\Middleware\Api;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Laravel\Models\Establishment;
use Input;

class ValidEstablishment {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$establishment_id = Input::get('establishment_id',0);
		$establishment = Establishment::select("id")->where('id',$establishment_id)->where('status',"published")->first();

		$directory_id = Input::get('directory_id',0);
		$directory = Establishment::where('directory_id',$directory_id)->where('status',"published");

		if(!$directory && !$establishment){
			$response = array(
					"msg" => "Record not found.",
					"status" => FALSE,
					'status_code' => "RECORD_NOT_FOUND"
				);
			$response_code = 404;

			return response($response, $response_code);
		}

		return $next($request);
	}

}
