<?php

namespace App\Laravel\Middleware\Api;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Laravel\Models\SubService;
use Input;

class ValidSubService {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$subservice_id = Input::get('subservice_id',0);
		$service = SubService::select("id")->where('id',$subservice_id)->where('status',"published")->first();

		if(!$service){
			$response = array(
					"msg" => "Record not found.",
					"status" => FALSE,
					'status_code' => "RECORD_NOT_FOUND"
				);
			$response_code = 404;

			return response($response, $response_code);
		}

		return $next($request);
	}

}
