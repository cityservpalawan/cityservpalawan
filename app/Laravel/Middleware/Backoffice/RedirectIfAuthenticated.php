<?php 

namespace App\Laravel\Middleware\Backoffice;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;

class RedirectIfAuthenticated {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->auth->check() AND in_array($this->auth->user()->type, ["super_user","admin","receptionist","tv","teller"]))
		{
			switch ($user->type) {
				case 'tv': return new RedirectResponse(route('backoffice.queue.tv')); break;
				case 'receptionist': return new RedirectResponse(route('backoffice.queue.index')); break;
				case 'teller': return new RedirectResponse(route('backoffice.queue.queue')); break;
				default: return new RedirectResponse(route('backoffice.dashboard')); break;
			}
		}

		return $next($request);
	}

}