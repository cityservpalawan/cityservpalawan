<?php

$router->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "Backoffice", 
	'as' => "backoffice.", 
	// 'domain' => env('BACKOFFICE_URL',"backoffice.pepero.localhost.com"), 
	'middleware'=> "web"

	], function(){

	$this->get('activate/{activation_token?}', ['as' => "activate", 'uses' => "AuthController@activate"]);
		
	/**
	*
	* Routes for guests
	*/
	$this->group(['middleware' => "backoffice.guest"], function(){
		$this->get('login',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login',['uses' => "AuthController@authenticate"]);
	});

	/**
	*
	* Routes for authenticated users
	*/
	$this->group(['middleware' => "backoffice.auth"], function(){
		
		$this->get('lock',['as' => "lock",'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		$this->group(['middleware' => ["backoffice.lock","backoffice.authorize"] ], function(){
			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);

			$this->group(['prefix' => "admins", 'as' => "admins."], function(){
				$this->get('/',['as' => "index",'uses' => "AdminController@index"]);
				$this->get('create',['as' => "create",'uses' => "AdminController@create"]);
				$this->post('create',['uses' => "AdminController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "AdminController@edit"]);
				$this->post('edit/{id?}',['uses' => "AdminController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AdminController@destroy"]);
			});

			$this->group(['prefix' => "app-settings", 'as' => "app_settings."], function(){
				$this->get('/',['as' => "index",'uses' => "AppSettingController@index"]);
				$this->get('create',['as' => "create",'uses' => "AppSettingController@create"]);
				$this->post('create',['uses' => "AppSettingController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "AppSettingController@edit"]);
				$this->post('edit/{id?}',['uses' => "AppSettingController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AppSettingController@destroy"]);
			});

			$this->group(['prefix' => "schools", 'as' => "schools."], function(){
				$this->get('/',['as' => "index",'uses' => "SchoolController@index"]);
				$this->get('create',['as' => "create",'uses' => "SchoolController@create"]);
				$this->post('create',['uses' => "SchoolController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "SchoolController@edit"]);
				$this->post('edit/{id?}',['uses' => "SchoolController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SchoolController@destroy"]);
			});

			$this->group(['prefix' => "articles", 'as' => "articles."], function(){
				$this->get('/',['as' => "index",'uses' => "ArticleController@index"]);
				$this->get('create',['as' => "create",'uses' => "ArticleController@create"]);
				$this->post('create',['uses' => "ArticleController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "ArticleController@edit"]);
				$this->post('edit/{id?}',['uses' => "ArticleController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ArticleController@destroy"]);
			});

			$this->group(['prefix' => "announcements", 'as' => "announcements."], function(){
				$this->get('/',['as' => "index",'uses' => "AnnouncementController@index"]);
				$this->get('create',['as' => "create",'uses' => "AnnouncementController@create"]);
				$this->post('create',['uses' => "AnnouncementController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "AnnouncementController@edit"]);
				$this->post('edit/{id?}',['uses' => "AnnouncementController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AnnouncementController@destroy"]);
			});

			$this->group(['prefix' => "polls", 'as' => "polls."], function(){
				$this->get('/',['as' => "index",'uses' => "PollController@index"]);
				$this->get('create',['as' => "create",'uses' => "PollController@create"]);
				$this->post('create',['uses' => "PollController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "PollController@edit"]);
				$this->post('edit/{id?}',['uses' => "PollController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PollController@destroy"]);
			});

			$this->group(['prefix' => "events", 'as' => "events."], function () {
				$this->get('/',['as' => "index", 'uses' => "AppEventController@index"]);
				$this->get('create',['as' => "create", 'uses' => "AppEventController@create"]);
				$this->post('create',['uses' => "AppEventController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AppEventController@edit"]);
				$this->post('edit/{id?}',['uses' => "AppEventController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AppEventController@destroy"]);
			});

			$this->group(['prefix' => "citizen", 'as' => "users."], function(){
				$this->get('/',['as' => "index",'uses' => "UserController@index"]);
				$this->get('create',['as' => "create",'uses' => "UserController@create"]);
				$this->post('create',['uses' => "UserController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "UserController@edit"]);
				$this->post('edit/{id?}',['uses' => "UserController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "UserController@destroy"]);
				//$this->any('new-activation-code/{id?}',['as' => "new_activation_code", 'uses' => "UserController@new_activation_code"]);
				//$this->any('resend-activation-code/{id?}',['as' => "resend_activation_code", 'uses' => "UserController@resend_activation_code"]);
			});

			$this->group(['prefix' => "citizen-reports", 'as' => "citizen_reports."], function(){
				$this->get('/',['as' => "index",'uses' => "CitizenReportController@index"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "CitizenReportController@edit"]);
				$this->post('edit/{id?}',['uses' => "CitizenReportController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "CitizenReportController@destroy"]);
			});

			$this->group(['prefix' => "death-certificate-requests", 'as' => "death_certificate_requests."], function(){
				$this->get('/',['as' => "index",'uses' => "DeathCertificateController@index"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "DeathCertificateController@edit"]);
				$this->post('edit/{id?}',['uses' => "DeathCertificateController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "DeathCertificateController@destroy"]);
			});

			$this->group(['prefix' => "marriage-certificate-requests", 'as' => "marriage_certificate_requests."], function(){
				$this->get('/',['as' => "index",'uses' => "MarriageCertificateController@index"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "MarriageCertificateController@edit"]);
				$this->post('edit/{id?}',['uses' => "MarriageCertificateController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "MarriageCertificateController@destroy"]);
			});

			$this->group(['prefix' => "prior-adoption-requests", 'as' => "prior_adoption_requests."], function(){
				$this->get('/',['as' => "index",'uses' => "PriorAdoptionController@index"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "PriorAdoptionController@edit"]);
				$this->post('edit/{id?}',['uses' => "PriorAdoptionController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PriorAdoptionController@destroy"]);
			});

			$this->group(['prefix' => "after-adoption-requests", 'as' => "after_adoption_requests."], function(){
				$this->get('/',['as' => "index",'uses' => "AfterAdoptionController@index"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "AfterAdoptionController@edit"]);
				$this->post('edit/{id?}',['uses' => "AfterAdoptionController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AfterAdoptionController@destroy"]);
			});

			$this->group(['prefix' => "directories", 'as' => "directories."], function(){
				$this->get('/',['as' => "index",'uses' => "DirectoryController@index"]);
				$this->get('create',['as' => "create",'uses' => "DirectoryController@create"]);
				$this->post('create',['uses' => "DirectoryController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "DirectoryController@edit"]);
				$this->post('edit/{id?}',['uses' => "DirectoryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "DirectoryController@destroy"]);
			});

			$this->group(['prefix' => "establishments", 'as' => "establishments."], function(){
				$this->get('/',['as' => "index",'uses' => "EstablishmentController@index"]);
				$this->get('create',['as' => "create",'uses' => "EstablishmentController@create"]);
				$this->post('create',['uses' => "EstablishmentController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "EstablishmentController@edit"]);
				$this->post('edit/{id?}',['uses' => "EstablishmentController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EstablishmentController@destroy"]);
			});

			$this->group(['prefix' => "emergencies", 'as' => "emergencies."], function(){
				$this->get('/',['as' => "index",'uses' => "EmergencyController@index"]);
				$this->get('create',['as' => "create",'uses' => "EmergencyController@create"]);
				$this->post('create',['uses' => "EmergencyController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "EmergencyController@edit"]);
				$this->post('edit/{id?}',['uses' => "EmergencyController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EmergencyController@destroy"]);
			});

			$this->group(['prefix' => "services", 'as' => "services."], function(){
				$this->get('/',['as' => "index",'uses' => "ServiceController@index"]);
				$this->get('create',['as' => "create",'uses' => "ServiceController@create"]);
				$this->post('create',['uses' => "ServiceController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "ServiceController@edit"]);
				$this->post('edit/{id?}',['uses' => "ServiceController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ServiceController@destroy"]);
			});

			$this->group(['prefix' => "sub-services", 'as' => "subservices."], function(){
				$this->get('/',['as' => "index",'uses' => "SubServiceController@index"]);
				$this->get('create',['as' => "create",'uses' => "SubServiceController@create"]);
				$this->post('create',['uses' => "SubServiceController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "SubServiceController@edit"]);
				$this->post('edit/{id?}',['uses' => "SubServiceController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SubServiceController@destroy"]);
			});

			$this->group(['prefix' => "pages", 'as' => "pages."], function(){
				$this->get('/',['as' => "index",'uses' => "PageController@index"]);
				$this->get('create',['as' => "create",'uses' => "PageController@create"]);
				$this->post('create',['uses' => "PageController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "PageController@edit"]);
				$this->post('edit/{id?}',['uses' => "PageController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PageController@destroy"]);
			});

			$this->group(['prefix' => "moments", 'as' => "moments."], function(){
				$this->get('/',['as' => "index",'uses' => "MomentController@index"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "MomentController@edit"]);
				$this->post('edit/{id?}',['uses' => "MomentController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "MomentController@destroy"]);
			});

			$this->group(['prefix' => "widgets", 'as' => "widgets."], function(){
				$this->get('/',['as' => "index",'uses' => "WidgetController@index"]);
				$this->get('create',['as' => "create",'uses' => "WidgetController@create"]);
				$this->post('create',['uses' => "WidgetController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "WidgetController@edit"]);
				$this->post('edit/{id?}',['uses' => "WidgetController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "WidgetController@destroy"]);
			});

			$this->group(['prefix' => "queue", 'as' => "queue."], function(){
				$this->get('/',['as' => "index",'uses' => "QueueController@index"]);
				$this->get('tv',['as' => "tv",'uses' => "QueueController@tv"]);
				$this->get('queue',['as' => "queue",'uses' => "QueueController@queue"]);
				$this->get('ticket/{queue_no?}',['as' => "ticket",'uses' => "QueueController@ticket"]);
			});

			$this->group(['prefix' => "surveys", 'as' => "surveys."], function(){
				$this->get('/',['as' => "index",'uses' => "SurveyController@index"]);
				$this->get('show/{id?}',['as' => "show",'uses' => "SurveyController@show"]);
			});

			$this->group(['prefix' => "report-footers", 'as' => "report_footers."], function(){
				$this->get('/',['as' => "index",'uses' => "ReportFooterController@index"]);
				$this->get('create',['as' => "create",'uses' => "ReportFooterController@create"]);
				$this->post('create',['uses' => "ReportFooterController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "ReportFooterController@edit"]);
				$this->post('edit/{id?}',['uses' => "ReportFooterController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ReportFooterController@destroy"]);
			});

			$this->group(['prefix' => "mayors-action-center", 'as' => "citizen_requests."], function(){
				$this->get('/',['as' => "index",'uses' => "CitizenRequestController@index"]);
				$this->get('create/{id?}',['as' => "create",'uses' => "CitizenRequestController@create"]);
				$this->post('create/{id?}',['uses' => "CitizenRequestController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "CitizenRequestController@edit"]);
				$this->post('edit/{id?}',['uses' => "CitizenRequestController@update"]);
				$this->get('transaction/{id?}',['as' => "transaction",'uses' => "CitizenRequestController@transaction"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "CitizenRequestController@destroy"]);
				$this->get('tracker/{id?}',['as' => "tracker_edit",'uses' => "CitizenRequestController@tracker_edit"]);
				$this->post('tracker/{id?}',['uses' => "CitizenRequestController@tracker_update"]);
				$this->any('quick-update/{id?}',['as' => "tracker_quick_update", 'uses' => "CitizenRequestController@tracker_quick_update"]);
			});
			
			$this->group(['prefix' => "reports", 'as' => "reports."], function(){
				$this->get('/',['as' => "index",'uses' => "ReportController@index"]);
				$this->get('generate',['as' => "generate",'uses' => "ReportController@generate"]);
			});
		});
	});
});
