<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class PriorAdoptionRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'status' => "required"
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}