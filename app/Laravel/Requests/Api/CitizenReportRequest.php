<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class CitizenReportRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'type' => 'required|exists:emergency,title',
			'content' => 'required',
			'file' => "image",
		];
	}

	public function messages(){
		return [
			'required' => "Field is required.",
			'type.exists' => "Invalid emergency type"
		];
	}
}