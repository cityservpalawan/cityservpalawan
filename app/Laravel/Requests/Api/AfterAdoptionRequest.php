<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class AfterAdoptionRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'req_address' => 'required',
			'lname' => 'required',
			'fname' => 'required',
			'middle_name' => 'required',
			'father_lname' => 'required',
			'father_fname' => 'required',
			'father_middle_name' => 'required',
			'father_occupation' => 'required',
			'father_religion' => 'required',
			'father_age' => 'required',
			'mother_lname' => 'required',
			'mother_fname' => 'required',
			'mother_middle_name' => 'required',
			'mother_occupation' => 'required',
			'mother_religion' => 'required',
			'mother_age' => 'required',
			'contact' => 'required',

			'decree_issued' => 'required',
			'final_decree' => 'required',
			'court_name' => 'required',
			'judge_lname' => 'required',
			'judge_fname' => 'required',
			'judge_middle_name' => 'required',
			'purpose' => 'required',
			'number_of_copies' => 'required'

		];
	}

	public function messages(){
		return [
			'required' => "Field is required."
		];
	}
}