<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class Request extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			
		];
	}

	public function messages(){
		return [
			
		];
	}
}