<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class TrackerRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		$rules = [
			'tracking_id' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "Field is required.",
		];
	}
}