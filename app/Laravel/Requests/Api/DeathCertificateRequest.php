<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class DeathCertificateRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'req_address' => 'required',
			'lname' => 'required',
			'fname' => 'required',
			'middle_name' => 'required',
			'contact' => 'required',
			'place_of_death' => 'required',
			'date_of_death' => 'required',
			'purpose' => 'required',
			'number_of_copies' => 'required'

		];
	}

	public function messages(){
		return [
			'required' => "Field is required."
		];
	}
}