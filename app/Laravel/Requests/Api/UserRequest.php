<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class UserRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		$rules = [
			'email' => 'required|email|unique_email',
			'fname' => "required",
			'lname' => "required",
			'contact_number'	=> "required",
		];

		if(!$this->has('fb_id')){
			$rules['password'] = "required|password_format|between:6,25|confirmed";
		}else{
			unset($rules['email']);
		}

		return $rules;
	}

	public function messages(){
		return [
			'password_format'			=> "Invalid password format.",
			'unique_email'				=> "Email address already taken",
			'required'					=> "Field is required.",
			'password.between'			=> "Password must be min. of 6 and max. of 25 characters.",
			'password.confirmed'		=> "Password does not match.",
		];
	}

}