<?php 

namespace App\Laravel\Traits;

use Carbon, Str;

trait StatusTrait{

	/**
	 * Parse data to an html <badge>
	 *
	 * @param string $status
	 *
	 * @return Date
	 */
	public function status_badge($col) {
		$status = Str::lower($this->{$col});
		switch ($status) {
			case 'pending': $badge = "<span class='label label-block label-striped border-left-grey'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'done': $badge = "<span class='label label-block label-striped border-left-success'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'rejected': $badge = "<span class='label label-block label-striped border-left-danger'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			case 'for_revalidation': $badge = "<span class='label label-block label-striped border-left-violet'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
			default: $badge = "<span class='label label-block label-striped border-left-default'>" . Str::upper(str_replace("_", " ", $status)) . "</span>"; break;
		}

		return $badge;
	}

}