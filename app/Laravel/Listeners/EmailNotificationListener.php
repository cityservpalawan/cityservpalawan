<?php namespace App\Laravel\Listeners;

use App\Laravel\Events\EmailNotification;

class EmailNotificationListener{

	public function handle(EmailNotification $email){
		$email->job();
	}
}