<?php namespace App\Laravel\Listeners;

use App\Laravel\Events\EmailForgotPassword;

class ForgotPasswordListener{

	public function handle(EmailForgotPassword $email){
		$email->job();
	}
}