<?php namespace App\Laravel\Listeners;

use App\Laravel\Events\LogCitizenRequest;

class LogCitizenRequestListener{

	public function handle(LogCitizenRequest $log){
		$log->job();
	}
}