<?php namespace App\Laravel\Listeners;

use App\Laravel\Events\PushNotification;

class PushNotificationListener{

	public function handle(PushNotification $notify){
		$notify->job();
	}
}