<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\PriorAdoption;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class PriorAdoptionTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date','user'
    ];

	public function transform(PriorAdoption $request){
	     return [
	     	'id' => $request->id,
	     	'code' => $request->code,
	     	'lname' => $request->lname,
	     	'fname' => $request->fname,
	     	'middle_name' => $request->middle_name,
	     	'father_last_name'	=> $request->father_lname,
	     	'father_first_name' => $request->father_fname,
	     	'father_middle_name' => $request->father_middle_name,
	     	'mother_maiden_last_name'	=> $request->mother_maiden_lname,
	     	'mother_maiden_first_name' => $request->mother_maiden_fname,
	     	'mother_maiden_middle_name' => $request->mother_maiden_middle_name,
	     	'date_of_birth' => $request->date_of_birth,
	     	'place_of_birth' => $request->place_of_birth,
	     	'contact_number' => $request->contact,
	     	// 'purpose' => $request->purpose,
	     	// 'number_of_copies' => $request->number_of_copies,
	     	
	     	'requested_by' => $request->author ? : new User,
	     ];
	}

	public function includeDate(PriorAdoption $request){
        $collection = Collection::make([
			'date_db' => $request->date_db($request->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $request->month_year($request->created_at),
			'time_passed' => $request->time_passed($request->created_at),
			'timestamp' => $request->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(PriorAdoption $request){
		$collection = Collection::make([
			'sender'	=> $request->author ? "{$request->author->fname} {$request->author->lname}" : "Anonymous",
			'status'	=> $request->status,
			'code'	=> $request->code,
			'content' => $request->content,
			'geolocation'	=> [
    			"lat"			=> $request->geo_long,
    			"long"			=> $request->geo_lat,
    		],
 			'path' => $request->path,
 			'directory' => $request->directory,
 			'full_path' => $request->path ? "{$request->directory}/resized/{$request->filename}" : asset("{$request->directory}/resized/{$request->filename}"),
 			'thumb_path' => $request->path ? "{$request->directory}/thumbnails/{$request->filename}" : asset("{$request->directory}/thumbnails/{$request->filename}"),
		]);
		return $this->item($collection, new MasterTransformer);
	}

	public function includeUser(PriorAdoption $request){
       $user = $request->author ? : new User;
       if(is_null($user->id)){ $user->id = 0;}
       return $this->item($user, new UserTransformer);
    }
}