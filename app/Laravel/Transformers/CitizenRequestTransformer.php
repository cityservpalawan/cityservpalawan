<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\CRTrackerHeader;
use App\Laravel\Models\CitizenRequest;
use App\Laravel\Models\MacRequest;
use App\Laravel\Models\Survey;
use App\Laravel\Transformers\CitizenRequestLogTransformer;
use App\Laravel\Transformers\MacRequestTransformer;
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TrackerTransformer;
use DB,Helper,Str,Cache,Carbon,Input;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class CitizenRequestTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date','logs','survey'
    ];

	public function transform(CitizenRequest $request){
	     return [
	     	'id' => $request->id,
	     	'user_id' => $request->user_id,
			'code' => $request->tracking_number,
			'tracking_number' => $request->tracking_number,
			'title' => $request->title,
			'name' => $request->name,
			'email' => $request->email,
			'contact_number' => $request->contact_number,
			// 'target_table' => $request->target_table,
			'type' => $request->type,
			'sub_type' => $request->sub_type,
			'remarks' => $request->remarks,
			'status' => $request->status,
	     ];
	}

	public function includeDate(CitizenRequest $request){
        $collection = Collection::make([
			'date_db' => $request->date_db($request->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $request->month_year($request->created_at),
			'time_passed' => $request->time_passed($request->created_at),
			'timestamp' => $request->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeSurvey(CitizenRequest $request){
		$user_id = Input::get('auth_id');
		$survey = Survey::where('citizen_request_id', $request->id)->where('user_id', $user_id)->first();

        $collection = Collection::make([
			'survey_exist' => $survey ? TRUE : FALSE,
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(CitizenRequest $request){

		return $this->item($request->tracker ? : new CRTrackerHeader, new TrackerTransformer);
		// switch ($request->type) {
		// 	case 'mac':
		// 		return $this->item($request->mac_info ? : new MacRequest , new MacRequestTransformer);
		// 	break;
			
		// 	default:
		// 		return array();
		// 	break;
		// }
	}

	public function includeLogs(CitizenRequest $request){
		return $this->collection($request->logs, new CitizenRequestLogTransformer);
	}
}