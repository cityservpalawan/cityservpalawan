<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\QueueForDisplay;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class QueueForDisplayTransformer extends TransformerAbstract{


	public function transform(QueueForDisplay $for_display){
	     return [
	     	'id' => $for_display->id,
	     	'queue_id' => $for_display->queue_id,
			'queue_no' => $for_display->queue_no,
			'is_priority' => $for_display->is_priority,
			'type' => $for_display->type,
			'status' => $for_display->status,
	     ];
	}

}