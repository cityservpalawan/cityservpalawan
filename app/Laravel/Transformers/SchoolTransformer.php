<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\School;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class SchoolTransformer extends TransformerAbstract{

	
	public function transform(School $school){
	     return [
	     	'id' => $school->id,
	     	'title' => $school->title,	
	     	
	     ];
	}

	public function includeDate(School $school){
        $collection = Collection::make([
			'date_db' => $school->date_db($school->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $school->month_year($school->created_at),
			'time_passed' => $school->time_passed($school->created_at),
			'timestamp' => $school->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(School $school){
		$user_id = Input::get('auth_id',0);
		$collection = Collection::make([
			'id' => $school->id,
			'title' => $school->title,
 			
		]);
		return $this->item($collection, new MasterTransformer);
	}
}