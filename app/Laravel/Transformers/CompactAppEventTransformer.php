<?php 

namespace App\Laravel\Transformers;

use App\Laravel\Models\AppEvent;
use League\Fractal\TransformerAbstract;

class CompactAppEventTransformer extends TransformerAbstract{

	public function transform(AppEvent $event)
	{
	    return [
			"day"		=> $event->date_format($event->posted_at,'d') + 0,
			"month" 	=> $event->date_format($event->posted_at,'m') + 0,
			"year"		=> $event->date_format($event->posted_at,'Y') + 0,
			"date"		=> $event->date_db($event->posted_at),
	    ];
	}
	
}