<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\User;


use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class UserTransformer extends TransformerAbstract{

	protected $availableIncludes = [
        'date','info','avatar'
    ];

	public function transform(User $user){
	     return [
	     	'id' => $user->id,
	     	'username' => $user->username,	
	     	'email' => $user->email,
	     	'type' => $user->type,
	     	'fb_id' => $user->fb_id
	     ];
	}

	public function includeDate(User $user){
        $collection = Collection::make([
    			'member_since'	=> [
    				'date_db' => $user->date_db($user->created_at,env("MASTER_DB_DRIVER","mysql")),
    				'month_year' => $user->month_year($user->created_at),
    				'time_passed' => $user->time_passed($user->created_at),
    				'timestamp' => $user->created_at
    			],
        	]);

        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(User $user){
		$collection = Collection::make([
			'fname' => $user->fname,
			'lname' => $user->lname,
			'name' => "{$user->fname} {$user->lname}",
			'contact_number' => $user->contact_number,
     		'gender' => $user->gender,
     		'age' => $user->age,
     		'birthdate' => $user->birthdate,
     		'barangay' => $user->barangay,
		]);

		return $this->item($collection, new MasterTransformer);
	}

	public function includeAvatar(User $user){
		$collection = Collection::make([
			'path' => $user->directory,
 			'filename' => $user->filename,
 			'path' => $user->path,
 			'directory' => $user->directory,
 			'full_path' => $user->path ? "{$user->directory}/resized/{$user->filename}" : asset("{$user->directory}/resized/{$user->filename}"),
 			'thumb_path' => $user->path ? "{$user->directory}/thumbnails/{$user->filename}" : asset("{$user->directory}/thumbnails/{$user->filename}"),
		]);

		return $this->item($collection, new MasterTransformer);
	}

}