<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Moment;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class MomentTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date','user'
    ];

	public function transform(Moment $moment){
	     return [
	     	'id' => $moment->id,
	     	'excerpt' => Helper::get_excerpt($moment->content),
	     ];
	}

	public function includeDate(Moment $moment){
        $collection = Collection::make([
			'date_db' => $moment->date_db($moment->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $moment->month_year($moment->created_at),
			'time_passed' => $moment->time_passed($moment->created_at),
			'timestamp' => $moment->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(Moment $moment){
		$collection = Collection::make([
			'sender'	=> $moment->author ? "{$moment->author->fname} {$moment->author->lname}" : "Anonymous",
			'status'	=> $moment->status,
			'content' => $moment->content,
 			'path' => $moment->path,
 			'directory' => $moment->directory,
 			'full_path' => $moment->path ? "{$moment->directory}/resized/{$moment->filename}" : asset("{$moment->directory}/resized/{$moment->filename}"),
 			'thumb_path' => $moment->path ? "{$moment->directory}/thumbnails/{$moment->filename}" : asset("{$moment->directory}/thumbnails/{$moment->filename}"),
		]);
		return $this->item($collection, new MasterTransformer);
	}

	public function includeUser(Moment $moment){
       $user = $moment->author ? : new User;
       if(is_null($user->id)){ $user->id = 0;}
       return $this->item($user, new UserTransformer);
    }
}