<?php 

namespace App\Laravel\Transformers;

use App\Laravel\Models\Establishment;
use App\Laravel\Models\Directory;

use App\Laravel\Transformers\DirectoryTransformer;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

class EstablishmentTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'date','directory'
    ];

	public function transform(Establishment $directory)
	{
	    return [
	    	'id' => $directory->id,
	    	'directory_id'			=> $directory->directory_id,
	    	'name'		=> $directory->name,
	    	'address'		=> $directory->address ? : "???",
	    	'contact'		=> $directory->contact ? : "???",
	    	'geolocation'	=> [
    			"lat"	=> $directory->geo_lat,
    			"long"	=> $directory->geo_long,
    		]
	    ];
	}

	public function includeDate(Establishment $directory){
        $collection = Collection::make([
			'date_db' => $directory->date_db($directory->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $directory->month_year($directory->created_at),
			'time_passed' => $directory->time_passed($directory->created_at),
			'timestamp' => $directory->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeDirectory(Establishment $directory){
        $directory = $directory->directory ? : new Directory;
        if(!$directory->id) $directory->id = 0;
        return $this->item($directory, new DirectoryTransformer);
	}
	
}