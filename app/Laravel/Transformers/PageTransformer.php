<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Page;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class PageTransformer extends TransformerAbstract{


	public function transform(Page $page){
	     return [
	     	'id' => $page->id,
	     	'title' => $page->title,
	     	'code' => $page->code,
	     	'slug' => $page->slug,
	     	'excerpt' => $page->excerpt,
	     	'content' => $page->content,
	     ];
	}

}