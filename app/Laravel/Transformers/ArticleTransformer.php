<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Article;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class ArticleTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date'
    ];

	public function transform(Article $article){
	     return [
	     	'id' => $article->id,
	     	'title' => $article->title,	
	     	'excerpt' => $article->excerpt,
	     ];
	}

	public function includeDate(Article $article){
        $collection = Collection::make([
			'date_db' => $article->date_db($article->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $article->month_year($article->created_at),
			'time_passed' => $article->time_passed($article->created_at),
			'timestamp' => $article->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(Article $article){
		$user_id = Input::get('auth_id',0);
		$collection = Collection::make([
			'author' => $article->author ? "{$article->author->fname} {$article->author->lname}" : "Anonymous",
			'content' => $article->content,
 			'path' => $article->path,
 			'directory' => $article->directory,
 			'full_path' => $article->path ? "{$article->directory}/resized/{$article->filename}" : asset("{$article->directory}/resized/{$article->filename}"),
 			'thumb_path' => $article->path ? "{$article->directory}/thumbnails/{$article->filename}" : asset("{$article->directory}/thumbnails/{$article->filename}"),
		]);
		return $this->item($collection, new MasterTransformer);
	}
}