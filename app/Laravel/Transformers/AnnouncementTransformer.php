<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Announcement;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class AnnouncementTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date'
    ];

	public function transform(Announcement $announcement){
	     return [
	     	'id' => $announcement->id,
	     	'title' => $announcement->title,	
	     	'excerpt' => $announcement->excerpt,
	     ];
	}

	public function includeDate(Announcement $announcement){
        $collection = Collection::make([
			'date_db' => $announcement->date_db($announcement->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $announcement->month_year($announcement->created_at),
			'time_passed' => $announcement->time_passed($announcement->created_at),
			'timestamp' => $announcement->created_at
    	]);		
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(Announcement $announcement){
		$user_id = Input::get('auth_id',0);
		$collection = Collection::make([
			'author' => $announcement->author ? "{$announcement->author->fname} {$announcement->author->lname}" : "Anonymous",
			'content' => $announcement->content,
 			'path' => $announcement->path,
 			'directory' => $announcement->directory,
 			'full_path' => $announcement->path ? "{$announcement->directory}/resized/{$announcement->filename}" : asset("{$announcement->directory}/resized/{$announcement->filename}"),
 			'thumb_path' => $announcement->path ? "{$announcement->directory}/thumbnails/{$announcement->filename}" : asset("{$announcement->directory}/thumbnails/{$announcement->filename}"),
		]);
		return $this->item($collection, new MasterTransformer);
	}
}