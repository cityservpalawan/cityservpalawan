<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Queue;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class QueueTransformer extends TransformerAbstract{


	public function transform(Queue $queue){

		$service_type = $queue->service_type;
		$for_display_type = Helper::get_service_display($service_type);
		$icon_color = Helper::get_service_display($service_type,"icon_color");
		$icon = Helper::get_service_display($service_type,"icon");

		if($queue->is_priority == "yes"){
			$service_type = "priority_lane";
		}
	     return [
			'id'	=> $queue->id,
			'teller_id'	=> $queue->teller_id,
			'for_display'	=> $for_display_type,
			'icon_color' => $icon_color,
			'icon'	=> $icon,
	     	'service_type' => $service_type,
	     	'name'	=> $queue->name ? : "---",
			'sequence_no' => $queue->sequence_no,
			'queue_no' => $queue->queue_no,
			'status' => $queue->status,
			'source' => $queue->source,
			'is_priority' => $queue->is_priority,
			'is_transfer' => $queue->is_transfer,
			'created_at'	=> $queue->date_format($queue->created_at),
	     ];
	}

}
