<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class Widget extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'widget';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'title','code','status','path','filename','directory','parent_id','is_parent','type','display_screen','content','list_type','display_order','display_type','short_description' ];


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function scopeOn($query){
		return $query->where('status','on');
	}

	public function scopeOff($query){
		return $query->where('status','off');
	}

	public function parent(){
		return $this->belongsTo("App\Laravel\Models\Widget",'parent_id','id');
	}

	public function children(){
		return $this->hasMany("App\Laravel\Models\Widget",'parent_id','id');
	}
}