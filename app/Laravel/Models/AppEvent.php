<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class AppEvent extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'event';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'title',
		'excerpt',
		'venue',
		'content',
		'slug',
		'directory',
		'filename',
		'path',
		'geo_lat',
		'geo_long',
		'posted_at',
		'status',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function author(){
		return $this->belongsTo("App\Laravel\Models\User",'user_id','id');
	}
	
	public function scopePublished($query){
		return $query->where('status','published');
	}

	public function scopeDraft($query){
		return $query->where('status','draft');
	}
	
}