<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class CRTrackerHeader extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cr_tracker_header';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cr_category_id','cr_subcategory_id','citizen_request_id','category_title','category_code','subcategory_title','subcategory_code','appointment_schedule','release_amount'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = ['num_completed', 'num_process','percentage','total_duration'];

	public function getNumCompletedAttribute(){
        return $this->info->where('status',"done")->count();
    }

    public function getNumProcessAttribute(){
        return $this->info->count();
    }

    public function getPercentageAttribute(){
    	$percentage = 0;
    	if($this->num_process){
    		$percentage = ($this->num_completed/$this->num_process)*100;
    	}
    	return $percentage;
    }

    public function getTotalDurationAttribute(){
        return $this->info->sum('duration');
    }

	public function info(){
		return $this->hasMany("App\Laravel\Models\CRTracker",'cr_tracker_header_id','id');
	}

	public function citizen_request(){
		return $this->belongsTo("App\Laravel\Models\CitizenRequest",'citizen_request_id','id');
	}


}