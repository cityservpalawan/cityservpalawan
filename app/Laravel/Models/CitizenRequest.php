<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class CitizenRequest extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'citizen_request';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','code','name','email','contact_number','title','target_table','type','sub_type','remarks','status','category','subcategory','module','beneficiary_rel_to_client','beneficiary_income','beneficiary_occupation','beneficiary_education','beneficiary_address','beneficiary_religion','beneficiary_civil_status','beneficiary_age','beneficiary_birthplace','beneficiary_birthdate','beneficiary_gender','beneficiary_name','school_id'];

	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];
	
	public function author(){
		return $this->belongsTo("App\Laravel\Models\User",'user_id','id');
	}

	public function assessor_info(){
		switch ($this->sub_type) {
			case 'assessor_simple': return $this->hasOne("App\Laravel\Models\AssessorRequestSimple",'citizen_request_id','id');
			case 'assessor_with_taxmapping': return $this->hasOne("App\Laravel\Models\AssessorRequestWithTaxmapping",'citizen_request_id','id');
		}
	}

	public function mac_info(){
		return $this->hasOne("App\Laravel\Models\MacRequest",'citizen_request_id','id');
	}

	public function logs(){
		return $this->hasMany("App\Laravel\Models\CitizenRequestLog",'citizen_request_id','id')->orderBy('created_at',"DESC");
	}

	public function tracker(){
		return $this->hasone("App\Laravel\Models\CRTrackerHeader",'citizen_request_id','id');
	}
}