<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;
use App\Laravel\Traits\StatusTrait;

class CRTracker extends Model{
	
	use SoftDeletes, DateFormatterTrait, StatusTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cr_tracker';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cr_tracker_header_id','cr_process_id','process_title','process_code','from','to','duration','remarks','status',];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function admin(){
		return $this->belongsTo("App\Laravel\Models\User",'user_id','id');
	}

	public function header(){
		return $this->belongsTo("App\Laravel\Models\CRTrackerHeader",'cr_tracker_header_id','id');
	}
	
}