<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;
use App\Laravel\Traits\StatusTrait;

use Helper, Carbon;

class MacRequest extends Model{
	
	use SoftDeletes, DateFormatterTrait, StatusTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mac_request';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'citizen_request_id','schedule_appointment','final_interview',

		'info_validation_user_id',
		'info_validation_status',
		'info_validation_remarks',
		'info_validation_from',
		'info_validation_to',
		'info_validation_duration',

		'assessment_evaluation_user_id',
		'assessment_evaluation_status',
		'assessment_evaluation_remarks',
		'assessment_evaluation_from',
		'assessment_evaluation_to',
		'assessment_evaluation_duration',

		'final_interview_user_id',
		'final_interview_status',
		'final_interview_remarks',
		'final_interview_from',
		'final_interview_to',
		'final_interview_duration',

		'requirement_submission_user_id',
		'requirement_submission_status',
		'requirement_submission_remarks',
		'requirement_submission_from',
		'requirement_submission_to',
		'requirement_submission_duration',
	];

	

	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = ['num_completed','num_process','percentage','total_duration','process_flow'];



	public function getNumCompletedAttribute(){
        $count = 0;
        $cols = ['info_validation','assessment_evaluation','final_interview','requirement_submission'];
        foreach ($cols as $key => $field) {
        	if($this->{$field . '_status'} == "done") ++$count; 
        }
        return $count;
    }

    public function getProcessFlowAttribute(){
    	return ['info_validation','assessment_evaluation','final_interview','requirement_submission'];
    }

    public function getNumProcessAttribute(){
        $cols = ['info_validation','assessment_evaluation','final_interview','requirement_submission'];
        return count($cols);
    }

    public function getPercentageAttribute(){
    	$percentage = 0;
    	if($this->num_process){
    		$percentage = ($this->num_completed/$this->num_process)*100;
    	}
    	return $percentage;
    }

    public function getTotalDurationAttribute(){
        $sum = 0;
        $cols = ['info_validation','assessment_evaluation','final_interview','requirement_submission'];
        foreach ($cols as $key => $field) {
        	$sum +=  $this->{$field . '_duration'};
        }

        if($this->citizen_request AND $this->citizen_request->status == "pending"){
        	$from = $this->created_at;
			$to = Carbon::now();
			$sum = ceil((strtotime($to) - strtotime($from)) / 60);
        }
        
        return $sum;
    }
	
	private function _admin($foreign_key){
		return $this->belongsTo("App\Laravel\Models\User","{$foreign_key}_user_id",'id');
	}

	public function info_validation_admin() {
		return $this->_admin('info_validation');
	}

	public function assessment_evaluation_admin() {
		return $this->_admin('assessment_evaluation');
	}

	public function final_interview_admin() {
		return $this->_admin('final_interview');
	}

	public function requirement_submission_admin() {
		return $this->_admin('requirement_submission');
	}

	public function citizen_request(){
		return $this->belongsTo("App\Laravel\Models\CitizenRequest",'citizen_request_id','id');
	}

}