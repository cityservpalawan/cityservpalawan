<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\CitizenRequest;
use App\Laravel\Models\MacRequest;
use App\Laravel\Models\User;
use App\Laravel\Models\UserLog;
use App\Laravel\Models\CRModule;
use App\Laravel\Models\CRCategory;
use App\Laravel\Models\CRSubCategory;
use App\Laravel\Models\CRProcess;
use App\Laravel\Models\CRTracker;
use App\Laravel\Models\CRTrackerHeader;
use App\Laravel\Models\UserTracker;
use App\Laravel\Models\School;
use App\Laravel\Models\ReportFooter;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ReportRequest;

/**
*
* Additional classes needed by this controller
*/

use Helper, ImageUploader, Carbon, Session, Str, DB, Input, PDF;

class ReportController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$categories = CRCategory::orderBy('title',"ASC")->get();
		$subcategories = array();
		foreach ($categories as $index => $category) {
			$subcategories[$category->title] = CRSubcategory::where('cr_category_id', $category->id)->lists('title','code')->toArray();
		}
		$this->data['modules'] = ['' => "Choose module"] + CRModule::orderBy('title',"ASC")->lists('title','code')->toArray();
		$this->data['categories'] = ['' => "Choose category"] + $categories->lists('title', 'code')->toArray();
		$this->data['subcategories'] = ['' => "Choose MAC Category"] + $subcategories;
		$this->data['statuses'] = ['' => "Choose Status", 'completed' => "Completed", 'pending' => "Pending"];
		$this->data['schools'] = ['' => "Choose School"] + School::orderBy('title',"ASC")->lists('title','id')->toArray();
		$this->data['report_footers'] = ['' => "Choose Footer", '0' => "None"] + ReportFooter::orderBy('title',"ASC")->lists('title','id')->toArray();
	}

	public function index () {
		$now = Carbon::now();
		$date_range = Input::get('date_range', Helper::date_db($now) . " - " . Helper::date_db($now) );
		$this->data['date_range'] = $date_range;
		$this->data['subcategory'] = "";
		$this->data['status'] = "";
		$this->data['school_id'] = "";
		$this->data['report_footer_id'] = "";
		return view('backoffice.reports.index',$this->data);
	}

	public function generate (ReportRequest $request) {
		try {
			
			$subcategory = $request->get('subcategory');
			$status = $request->get('status');
			$school_id = $request->get('school_id');
			$report_footer_id = $request->get('report_footer_id');

			$now = Carbon::now();
			$date_range = Input::get('date_range', Helper::date_db($now) . " - " . Helper::date_db($now) );
			$date_range_array = explode(" - ", $date_range);
			
			if($date_range_array[0] == $date_range_array[1]) {
				$data_range_textual = Helper::date_format($date_range_array[0], "F d, Y");
			} else {
				$data_range_textual = Helper::date_format($date_range_array[0], "F d" ) . " - " . Helper::date_format($date_range_array[1], "F d, Y" );
			}

			$this->data['requests'] = CitizenRequest::whereHas('tracker', function($query) use ($date_range_array, $subcategory) {
											$query->whereBetween(DB::raw("DATE (appointment_schedule)"), $date_range_array)
												->where('subcategory_code', $subcategory);
										})
										->where(function($query) use($status) {
											if($status == "completed") {
												$query->where('status',"done");
											} else {
												$query->where('status',"pending");
											}
										})
										->where(function($query) use($subcategory, $school_id) {
											if($subcategory == "scholarship") {
												$query->where('school_id', $school_id);
											}
										})
										->orderBy('created_at', "DESC")
										->get();

			$this->data['total'] = 0.00;

			foreach ($this->data['requests'] as $key => $request) {
				$this->data['total'] += $request->tracker ? $request->tracker->release_amount : 0.00;
			}

			$cr_subcategory = CRSubcategory::where('code', $subcategory)->first();
			$school = School::where('id', $school_id)->first();
			$footer = ReportFooter::where('id', $report_footer_id)->first();

			if($subcategory == "scholarship") {
				$this->data['report_header'] = "BATANGAS CITY GOVERNMENT SCHOLARSHIP PROGRAM<br>" .
											   Str::upper($status) . " REQUESTS FOR " . Str::upper($data_range_textual);
				$this->data['report_title'] = $school->title;
			} else {
				$this->data['report_header'] = "BATANGAS CITY GOVERNMENT SOCIAL SERVICES (" . Str::upper($cr_subcategory->title) . ")<br>" .
											   Str::upper($status) . " REQUESTS FOR " . Str::upper($data_range_textual);
				$this->data['report_title'] = $cr_subcategory->title;
			}

			$this->data['report_footer'] = $footer ? $footer->content : NULL;
			$this->data['subcategory'] = $cr_subcategory;

			// return view('backoffice.reports.index',$this->data);
			$pdf = PDF::loadView('pdf.report', $this->data);
			return $pdf->setPaper('letter','landscape')->download('download.pdf');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	
}