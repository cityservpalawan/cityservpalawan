<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Poll;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\PollRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class PollController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['polls'] = Poll::orderBy('updated_at',"DESC")->get();
		return view('backoffice.polls.index',$this->data);
	}

	public function create () {
		return view('backoffice.polls.create',$this->data);
	}

	public function store (PollRequest $request) {
		try {
			$new_poll = new Poll;
			$new_poll->fill($request->all());
			$new_poll->posted_at = Helper::datetime_db($request->get('posted_at'));
			$new_poll->question = $request->get('question');
			$new_poll->start = $request->get('start');
			$new_poll->end = $request->get('end');

			$choices_metadata = [];

			if(is_array($request->get("keys")) AND is_array($request->get("values"))) {
				if(count($request->get("keys")) == count($request->get("values"))) {
					$counter = 0;
					
					foreach ($request->get("keys") as $index => $key) {
						if($key){
							$text = "choice";
							$count = $counter += 1;
							$choices_metadata += [
								$text => $key
							];
						}
					}
				}
			}	
			$encode_choice = json_encode($choices_metadata);
			$new_poll->choices_metadata = $encode_choice;

			if($request->hasFile('file')) $new_poll->fill(ImageUploader::upload($request, 'uploads/poll',"file"));

			if($new_poll->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A poll has been added.");
				return redirect()->route('backoffice.polls.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$poll = Poll::find($id);

		if (!$poll) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.polls.index');
		}

		$this->data['poll'] = $poll;
		return view('backoffice.polls.edit',$this->data);
	}

	public function update (PollRequest $request, $id = NULL) {
		try {
			$poll = Poll::find($id);

			if (!$poll) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.polls.index');
			}

			$poll->fill($request->all());
			$poll->posted_at = Helper::datetime_db($request->get('posted_at'));
			$poll->question = $request->get('question');
			
			if($request->hasFile('file')) $poll->fill(ImageUploader::upload($request, 'uploads/poll',"file"));

			if($poll->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A poll has been updated.");
				return redirect()->route('backoffice.polls.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$poll = Poll::find($id);

			if (!$poll) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.polls.index');
			}

			if($poll->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A poll has been deleted.");
				return redirect()->route('backoffice.polls.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}