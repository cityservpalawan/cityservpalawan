<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Widget;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\WidgetRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class WidgetController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['display_screens'] = [ '' => "Choose screen", 'none' => "None", 'home' => "Home", 'my_city' => "My City (Discover Paranaque)"];
		$this->data['types'] = [ '' => "Choose type", 'single_page' => "Single Page", 'list' => "List",  'calendar' => "Calendar", 'weather' => "Weather"];
		$this->data['list_types'] = [ '' => "Choose type of list", 'none' => "None", 'text_only' => "Text Only", 'image_only' => "Image Only", 'text_and_image' => "Text and Image"];
		$this->data['display_types'] = [ '' => "Choose type of display", 'none' => "None", 'text_only' => "Text Only", 'image_only' => "Image Only", 'text_and_image' => "Text and Image"];
		$this->data['statuses'] = [ '' => "Choose status", 'on' => "On", 'off' => "Off"];
		$this->data['parents'] = [ '' => "Choose parent widget", 0 => 'None'] + Widget::where('is_parent',1)->orderBy('title')->lists('title','id')->toArray();
	}

	public function index () {
		$this->data['widgets'] = Widget::orderBy('display_screen',"ASC")->orderBy('parent_id',"ASC")->orderBy('display_order',"ASC")->orderBy('updated_at',"DESC")->get();
		return view('backoffice.widgets.index',$this->data);
	}

	public function create () {
		return view('backoffice.widgets.create',$this->data);
	}

	public function store (WidgetRequest $request) {
		try {
			$new_widget = new Widget;
			$new_widget->fill($request->all());
			$new_widget->code = Helper::get_slug('widget','title',$request->get('title'));
			$new_widget->is_parent = ($request->get('is_parent',0) == 1) ? 1 : 0;
			$new_widget->parent_id = $request->get('parent_id',0);
			$new_widget->display_screen = $request->get('display_screen','none');
			$new_widget->list_type = $request->get('list_type','none');
			$new_widget->display_type = $request->get('display_type','none');
			$new_widget->display_order = $request->get('display_order',0);

			if($request->hasFile('file')) $new_widget->fill(ImageUploader::upload($request, 'uploads/widget',"file"));

			if($new_widget->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A widget has been added.");
				return redirect()->route('backoffice.widgets.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$widget = Widget::find($id);

		if (!$widget) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.widgets.index');
		}

		$this->data['widget'] = $widget;
		$this->data['parents'] = [ '' => "Choose parent widget", 0 => 'None'] + Widget::where('is_parent',1)->where('id','<>',$id)->orderBy('title')->lists('title','id')->toArray();
		return view('backoffice.widgets.edit',$this->data);
	}

	public function update (WidgetRequest $request, $id = NULL) {
		try {
			$widget = Widget::find($id);

			if (!$widget) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.widgets.index');
			}

			$widget->fill($request->except(['code']));
			$widget->is_parent = ($request->get('is_parent',0) == 1) ? 1 : 0;
			$widget->parent_id = $request->get('parent_id',0);
			$widget->display_screen = $request->get('display_screen','none');
			$widget->list_type = $request->get('list_type','none');
			$widget->display_type = $request->get('display_type','none');
			$widget->display_order = $request->get('display_order',0);

			if($request->hasFile('file')) $widget->fill(ImageUploader::upload($request, 'uploads/widget',"file"));

			if($widget->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A widget has been updated.");
				return redirect()->route('backoffice.widgets.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$widget = Widget::find($id);

			if (!$widget) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.widgets.index');
			}

			if($widget->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A widget has been deleted.");
				return redirect()->route('backoffice.widgets.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}