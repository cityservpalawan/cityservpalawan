<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Directory;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\DirectoryRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class DirectoryController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['directories'] = Directory::orderBy('updated_at',"DESC")->get();
		return view('backoffice.directories.index',$this->data);
	}

	public function create () {
		return view('backoffice.directories.create',$this->data);
	}

	public function store (DirectoryRequest $request) {
		try {
			$new_directory = new Directory;
			$new_directory->fill($request->all());
			if($request->hasFile('file')) $new_directory->fill(ImageUploader::upload($request, 'uploads/directory',"file"));

			if($new_directory->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A directory has been added.");
				return redirect()->route('backoffice.directories.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$directory = Directory::find($id);

		if (!$directory) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.directories.index');
		}

		$this->data['directory'] = $directory;
		return view('backoffice.directories.edit',$this->data);
	}

	public function update (DirectoryRequest $request, $id = NULL) {
		try {
			$directory = Directory::find($id);

			if (!$directory) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.directories.index');
			}

			$directory->fill($request->all());
			if($request->hasFile('file')) $directory->fill(ImageUploader::upload($request, 'uploads/directory',"file"));

			if($directory->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A directory has been updated.");
				return redirect()->route('backoffice.directories.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$directory = Directory::find($id);

			if (!$directory) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.directories.index');
			}

			if($directory->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A directory has been deleted.");
				return redirect()->route('backoffice.directories.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}