<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\CitizenReport;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\CitizenReportRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class CitizenReportController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['pending' => "Pending", 'on_going' => "On Going", "resolved" => "Resolved"];
	}

	public function index () {
		$this->data['reports'] = CitizenReport::orderBy('updated_at',"DESC")->get();
		return view('backoffice.citizen-reports.index',$this->data);
	}

	public function edit ($id = NULL) {
		$report = CitizenReport::find($id);

		if (!$report) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.citizen_reports.index');
		}

		$this->data['report'] = $report;
		$this->data['geo_lat'] = $report->geo_lat;
		$this->data['geo_long'] = $report->geo_long;
		return view('backoffice.citizen-reports.edit',$this->data);
	}

	public function update (CitizenReportRequest $request, $id = NULL) {
		try {
			$report = CitizenReport::find($id);

			if (!$report) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.citizen_reports.index');
			}

			$report->fill($request->all());
			// $report->posted_at = Helper::datetime_db($request->get('posted_at'));
			
			if($request->hasFile('file')) $report->fill(ImageUploader::upload($request, 'uploads/reports',"file"));

			if($report->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A report has been updated.");
				return redirect()->route('backoffice.citizen_reports.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$report = CitizenReport::find($id);

			if (!$report) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.citizen_reports.index');
			}

			if($report->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A report has been deleted.");
				return redirect()->route('backoffice.citizen_reports.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}