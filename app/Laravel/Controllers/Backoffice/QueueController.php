<?php

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Queue;

/**
*
* Requests used for validating inputs
*/

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB, PDF;

class QueueController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$current_date = Helper::date_db(Carbon::now());
		$this->data['total_queue'] = Queue::whereRaw("DATE(created_at) = '{$current_date}'")->count();
		$this->data['most_queue'] = Queue::select(DB::raw("COUNT(*) as sum_queue"),"service_type")->whereRaw("DATE(created_at) = '{$current_date}'")->groupBy("service_type")->orderByRaw("sum_queue DESC")->first();

		$this->data['recent_queue'] = Queue::whereRaw("DATE(created_at) = '{$current_date}'")->orderBy('created_at',"DESC")->take(5)->get();

		if(!$this->data['most_queue']){
			$this->data['most_queue'] = (object) array(
						'service_type' => "-",
						'sum_queue' => 0
				);
		}

		return view('backoffice.queue.request',$this->data);
	}

	public function ticket($queue_no = NULL){
		$current_date = Helper::date_db(Carbon::now());
		$this->data['queue'] = Queue::where('queue_no',$queue_no)->whereRaw("DATE(created_at) = '{$current_date}'")->first();

		if($this->data['queue']){
			$pdf = PDF::loadView('pdf.ticket', $this->data);
			return $pdf->setPaper(array(0,0,150, 250), 'landscape')->stream("{$this->data['queue']->queue_no}.pdf");	
		}

		return redirect()->route('backoffice.dashboard');
		
	}

	public function tv () {
		$current_date = Helper::date_db(Carbon::now());
		$this->data['priority_lane'] = Queue::where(function($query){
			return $query->where('status','completed')
						->orWhere('status','on_going');
		})->where('is_priority','yes')->whereRaw("DATE(created_at) = '{$current_date}'")->orderByRaw("FIELD(status,'on_going','completed')")->first();
		$this->data['health_service'] = Queue::where('is_priority','no')->where(function($query){
			return $query->where('status','completed')
						->orWhere('status','on_going');
		})->where('service_type','health_service')->whereRaw("DATE(created_at) = '{$current_date}'")->orderByRaw("FIELD(status,'on_going','completed')")->first();
		$this->data['scholarship_program'] = Queue::where('is_priority','no')->where(function($query){
			return $query->where('status','completed')
						->orWhere('status','on_going');
		})->where('service_type','scholarship_program')->whereRaw("DATE(created_at) = '{$current_date}'")->orderByRaw("FIELD(status,'on_going','completed')")->first();
		$this->data['social_service'] = Queue::where('is_priority','no')->where(function($query){
			return $query->where('status','completed')
						->orWhere('status','on_going');
		})->where('service_type','social_service')->whereRaw("DATE(created_at) = '{$current_date}'")->orderByRaw("FIELD(status,'on_going','completed')")->first();
		$this->data['legal_service'] = Queue::where('is_priority','no')->where(function($query){
			return $query->where('status','completed')
						->orWhere('status','on_going');
		})->where('service_type','legal_service')->whereRaw("DATE(created_at) = '{$current_date}'")->orderByRaw("FIELD(status,'on_going','completed')")->first();
		$this->data['administrative'] = Queue::where('is_priority','no')->where(function($query){
			return $query->where('status','completed')
						->orWhere('status','on_going');
		})->where('service_type','administrative')->whereRaw("DATE(created_at) = '{$current_date}'")->orderByRaw("FIELD(status,'on_going','completed')")->first();
		return view('backoffice.queue.tv',$this->data);
	}

	public function queue () {
		$current_date = Helper::date_db(Carbon::now());
		$this->data['queues'] = Queue::whereRaw("DATE(created_at) = '{$current_date}'")->where('service_type',"social_service")->where(function($query){
					return $query->where('status','pending')
								 ->orWhere('status','for_display')
								 ->orWhere('status','on_going');

		})->orderBy('created_at',"ASC")->get();
		return view('backoffice.queue.queue',$this->data);
	}

}
