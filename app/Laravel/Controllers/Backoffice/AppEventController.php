<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\AppEvent;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AppEventRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, ImageUploader;

class AppEventController extends Controller{

	/**
	*
	* @var array
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->auth = $this->data['auth'];
		$this->data['statuses'] = ['' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['events'] = AppEvent::orderBy('created_at',"DESC")->get();
		return view('backoffice.events.index',$this->data);
	}

	public function create () {
		$this->data['geo_lat'] = "14.5800";
		$this->data['geo_long'] = "121.0000";
		return view('backoffice.events.create',$this->data);
	}

	public function store (AppEventRequest $request) {
		try {
			$new_event = new AppEvent;
			$new_event->fill($request->all());
			$new_event->posted_at = Helper::datetime_db($request->get('posted_at'));
			$new_event->slug = Helper::get_slug('event','title',$request->get('title'));
			$new_event->excerpt = Helper::get_excerpt($request->get('content'));
			$new_event->user_id = $this->auth->id;

			if($request->hasFile('file')) $new_event->fill(ImageUploader::upload($request, 'uploads/event',"file"));

			if($new_event->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An event has been added.");
				return redirect()->route('backoffice.events.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$event = AppEvent::find($id);

		if (!$event) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.events.index');
		}

		$this->data['event'] = $event;
		$this->data['geo_lat'] = $event->geo_lat;
		$this->data['geo_long'] = $event->geo_long;
		return view('backoffice.events.edit',$this->data);
	}

	public function update (AppEventRequest $request, $id = NULL) {
		try {
			$event = AppEvent::find($id);

			if (!$event) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.events.index');
			}

			$old_title = $event->title;

			$event->fill($request->all());
			$event->posted_at = Helper::datetime_db($request->get('posted_at'));
			$event->excerpt = Helper::get_excerpt($request->get('content'));

			if($request->hasFile('file')) $event->fill(ImageUploader::upload($request, 'uploads/event',"file"));

			if($old_title != $request->get('title')){
				$event->slug = Helper::get_slug('event','title',$request->get('title'));
			}

			if($event->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An event has been updated.");
				return redirect()->route('backoffice.events.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$event = AppEvent::find($id);

			if (!$event) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.events.index');
			}

			if($event->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An event has been deleted.");
				return redirect()->route('backoffice.events.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}