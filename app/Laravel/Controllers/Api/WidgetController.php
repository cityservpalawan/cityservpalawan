<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;

use App\Laravel\Models\Widget;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\WidgetTransformer;

use Input, Str;


class WidgetController extends Controller{


	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);

		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function index($format = ""){
		try{
			$per_page = Input::get("per_page",10);
			$display_screen = Input::get('display_screen');

			$widgets = Widget::where('status',"on")->where('display_screen',$display_screen)->orderBy('display_order',"ASC")->where('is_parent', 1)->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($widgets,new WidgetTransformer,'collection');
			$this->response['has_morepage'] = $widgets->hasMorePages();
			$this->response['msg'] = "List of widgets.";
			$this->response['status_code'] = "WIDGET_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = ""){
		try{
			$widget_id = Input::get('widget_id');
			$widget = Widget::where('id',$widget_id)->first();

			$this->response['data'] = $this->transformer->transform($widget,new WidgetTransformer,'item');
			$this->response['msg'] = "Widget Details.";
			$this->response['status_code'] = "WIDGET_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}