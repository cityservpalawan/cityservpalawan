<?php 

namespace App\Laravel\Controllers\Api;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\School;

/**
*
* Requests used for this controller
*/

/**
*
* Transformers used for this controller
*/
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\SchoolTransformer;
use Helper, Carbon, Input, Str, ImageUploader;
use Request, GeoIp;

class SchoolController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}
	public function index($format = ""){
		try{
			$per_page = Input::get("per_page",10);
			$school = School::where('title',"Sample")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($school,new SchoolTransformer,'collection');
			$this->response['has_morepage'] = $school->hasMorePages();
			$this->response['msg'] = "List of schools.";
			$this->response['status_code'] = "ARTICLE_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}