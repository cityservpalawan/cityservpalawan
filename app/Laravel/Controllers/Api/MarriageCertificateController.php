<?php 

namespace App\Laravel\Controllers\Api;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\MarriageCertificate;
use App\Laravel\Models\User;

/**
*
* Requests used for this controller
*/
use App\Laravel\Requests\Api\MarriageCertificateRequest;

/**
*
* Transformers used for this controller
*/
use App\Laravel\Transformers\MarriageCertificateTransformer;

/**
*
* Classes used for this controller
*/
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use Helper, Carbon, Input, Str, ImageUploader;
use Request, GeoIp;

class MarriageCertificateController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	public function index($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$requests = MarriageCertificate::where('status','<>',"pending")
					->with('author')->orderBy('created_at',"DESC")
					->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($requests, new MarriageCertificateTransformer, 'collection');
			$this->response['has_morepage'] = $requests->hasMorePages();
			$this->response['msg'] = "My Request List.";
			$this->response['status_code'] = "MY_REQUEST_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function my_requests($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$requests = MarriageCertificate::with('author')
						->where('user_id',$this->user_id)
						->orderBy('created_at',"DESC")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($requests, new MarriageCertificateTransformer, 'collection');
			$this->response['has_morepage'] = $requests->hasMorePages();
			$this->response['msg'] = "My Report List.";
			$this->response['status_code'] = "MY_REPORT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = "json"){
		try{
			$request = MarriageCertificate::with('author')
						->where('id',Input::get('request_id',0))
						->first();

			$this->response['data'] = $this->transformer->transform($request, new MarriageCertificateTransformer, 'item');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function pending($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$request = MarriageCertificate::with('author')
						->where('status',"pending")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($request, new MarriageCertificateTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function on_going($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$request = MarriageCertificate::with('author')
						->where('status',"on_going")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($request, new MarriageCertificateTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function approved($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$request = MarriageCertificate::with('author')
						->where('status',"approved")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($request, new MarriageCertificateTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store(MarriageCertificateRequest $request, $format = "json"){
		try{	
			$new_request = new MarriageCertificate;
			$get_user = new User;
			$new_request->fill($request->all());

			$new_request->user_id = $this->user_id;

			$husband_lname = Input::get('husband_lname');
			$husband_fname = Input::get('husband_fname');
			$husband_middle_name = Input::get('husband_middle_name');
			$wife_lname = Input::get('wife_lname');
			$wife_fname = Input::get('wife_fname');
			$wife_middle_name = Input::get('wife_middle_name');
			$contact = Input::get('contact');
			$purpose = Input::get('purpose');
			$place_of_marriage = Input::get('place_of_marriage');
			$date_of_marriage = Input::get('date_of_marriage');
			$number_of_copies = Input::get('number_of_copies');
			$req_address = Input::get('req_address');


			$new_request->req_address = $req_address;
			$new_request->husband_lname = $husband_lname;
			$new_request->husband_fname = $husband_fname;
			$new_request->husband_middle_name = $husband_middle_name;
			$new_request->wife_lname = $wife_lname;
			$new_request->wife_fname = $wife_fname;
			$new_request->wife_middle_name = $wife_middle_name;
			$new_request->contact = $contact;
			$new_request->purpose = $purpose;
			$new_request->place_of_marriage = $place_of_marriage;
			$new_request->date_of_marriage = $date_of_marriage;
			$new_request->number_of_copies = $number_of_copies;


			if($request->hasFile('file')) $new_request->fill(ImageUploader::upload($request, "uploads/reports", "file"));

			if($new_request->save()) {
				$new_request->code = "MCR" . str_pad($new_request->id, 8, 0, STR_PAD_LEFT);
				$new_request->save();
				$this->response['msg'] = "Your report has been sent";
				$this->response['status_code'] = "REPORT_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function destroy($format = "json"){
		try{
			$request = MarriageCertificate::with('author')
						->where('id',Input::get('request_id',0))
						->first();

			$request->delete();

			$this->response['data'] = $this->transformer->transform($request, new MarriageCertificateTransformer, 'item');
			$this->response['msg'] = "Report  Deleted.";
			$this->response['status_code'] = "REPORT_DELETED";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}