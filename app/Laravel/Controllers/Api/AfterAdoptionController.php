<?php 

namespace App\Laravel\Controllers\Api;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\AfterAdoption;
use App\Laravel\Models\User;

/**
*
* Requests used for this controller
*/
use App\Laravel\Requests\Api\AfterAdoptionRequest;

/**
*
* Transformers used for this controller
*/
use App\Laravel\Transformers\AfterAdoptionTransformer;

/**
*
* Classes used for this controller
*/
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use Helper, Carbon, Input, Str, ImageUploader;
use Request, GeoIp;

class AfterAdoptionController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	public function index($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$requests = AfterAdoption::where('status','<>',"pending")
					->with('author')->orderBy('created_at',"DESC")
					->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($requests, new AfterAdoptionTransformer, 'collection');
			$this->response['has_morepage'] = $requests->hasMorePages();
			$this->response['msg'] = "Citizen Report List.";
			$this->response['status_code'] = "CITIZEN_REPORT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function my_requests($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$requests = AfterAdoption::with('author')
						->where('user_id',$this->user_id)
						->orderBy('created_at',"DESC")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($requests, new AfterAdoptionTransformer, 'collection');
			$this->response['has_morepage'] = $requests->hasMorePages();
			$this->response['msg'] = "My Report List.";
			$this->response['status_code'] = "MY_REPORT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = "json"){
		try{
			$request = AfterAdoption::with('author')
						->where('id',Input::get('request_id',0))
						->first();

			$this->response['data'] = $this->transformer->transform($request, new AfterAdoptionTransformer, 'item');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function pending($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$request = AfterAdoption::with('author')
						->where('status',"pending")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($request, new AfterAdoptionTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function on_going($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$request = AfterAdoption::with('author')
						->where('status',"on_going")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($request, new AfterAdoptionTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function approved($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$request = AfterAdoption::with('author')
						->where('status',"approved")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($request, new AfterAdoptionTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store(AfterAdoptionRequest $request, $format = "json"){
		try{	
			$new_request = new AfterAdoption;
			$get_user = new User;
			$new_request->fill($request->all());

			$new_request->user_id = $this->user_id;

			$lname = Input::get('lname');
			$fname  = Input::get('fname');
			$middle_name = Input::get('middle_name');
			$father_lname = Input::get('father_lname');
			$father_fname = Input::get('father_fname');
			$father_middle_name = Input::get('father_middle_name');
			$father_occupation = Input::get('father_occupation');
			$father_religion = Input::get('father_religion');
			$father_age = Input::get('father_age');
			$mother_lname = Input::get('mother_lname');
			$mother_fname = Input::get('mother_fname');
			$mother_middle_name = Input::get('mother_middle_name');
			$mother_occupation = Input::get('mother_occupation');
			$mother_religion = Input::get('mother_religion');
			$mother_age = Input::get('mother_age');
			$decree_issued = Input::get('decree_issued');
			$final_decree = Input::get('final_decree');
			$court_name = Input::get('court_name');
			$judge_lname = Input::get('judge_lname');
			$judge_fname = Input::get('judge_fname');
			$judge_middle_name = Input::get('judge_middle_name');
			$contact = Input::get('contact');
			$purpose = Input::get('purpose');
			// $place_of_birth = Input::get('place_of_birth');
			// $date_of_birth = Input::get('date_of_birth');
			$number_of_copies = Input::get('number_of_copies');
			$req_address = Input::get('req_address');


			$new_request->req_address = $req_address;
			$new_request->lname = $lname;
			$new_request->fname = $fname;
			$new_request->middle_name = $middle_name;
			$new_request->father_lname = $father_lname;
			$new_request->father_fname = $father_fname;
			$new_request->father_middle_name = $father_middle_name;
			$new_request->father_occupation = $father_occupation;
			$new_request->father_religion = $father_religion;
			$new_request->father_age = $father_age;
			$new_request->mother_lname = $mother_lname;
			$new_request->mother_fname = $mother_fname;
			$new_request->mother_middle_name = $mother_middle_name;
			$new_request->mother_occupation = $mother_occupation;
			$new_request->mother_religion = $mother_religion;
			$new_request->mother_age = $mother_age;
			$new_request->court_name = $court_name;
			$new_request->judge_lname = $judge_lname;
			$new_request->judge_fname = $judge_fname;
			$new_request->judge_middle_name = $judge_middle_name;
			$new_request->contact = $contact;
			$new_request->purpose = $purpose;
			// $new_request->place_of_birth = $place_of_birth;
			// $new_request->date_of_birth = $date_of_birth;
			$new_request->number_of_copies = $number_of_copies;


			if($request->hasFile('file')) $new_request->fill(ImageUploader::upload($request, "uploads/reports", "file"));

			if($new_request->save()) {
				$new_request->code = "AR" . str_pad($new_request->id, 8, 0, STR_PAD_LEFT);
				$new_request->save();
				$this->response['msg'] = "Your report has been sent";
				$this->response['status_code'] = "REPORT_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function destroy($format = "json"){
		try{
			$request = AfterAdoption::with('author')
						->where('id',Input::get('request_id',0))
						->first();

			$request->delete();

			$this->response['data'] = $this->transformer->transform($request, new AfterAdoptionTransformer, 'item');
			$this->response['msg'] = "Report  Deleted.";
			$this->response['status_code'] = "REPORT_DELETED";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}