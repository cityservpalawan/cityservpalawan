<?php

namespace App\Laravel\Services;

use Str, Curl, Exception;

class FCM {

	/**
	*
	* String
	* @var Application Key
	*/
	protected $app_key;

	/**
	*
	* Array
	* @var Data to be passed upon request
	*/
	protected $data;


	public function __construct() {
		$this->app_key = env("FCM_SERVER_KEY", FALSE);
	}

	private function _set_notification_data(array $notification) {
		if(!empty($notification)){
			$this->data['notification'] = [
				'title' => array_key_exists('title', $notification) ? $notification['title'] : NULL,
				'body' => array_key_exists('body', $notification) ? $notification['body'] : NULL,
				'icon' => array_key_exists('icon', $notification) ? $notification['icon'] : NULL,
			];
		}
	}

	private function _set_message_data(array $data) {
		if(!empty($data)){
			$this->data['data'] = $data;
		}
	}

	private function _set_key($recipient, $recipient_key = "to") {
		$recipient_key = Str::lower($recipient_key);
		$keys = ['to','condition'];

		if(!in_array($recipient_key, $keys)){
			throw new Exception("Recipient key not found");
		}

		$this->data["{$recipient_key}"] = $recipient;
	}

	private function _set_priority($priority = "normal") {
		$priority = Str::lower($priority);
		$keys = ['high','normal'];

		if(!in_array($priority, $keys)){
			throw new Exception("Priority not found");
		}

		$this->data["priority"] = "normal";
	}

	private function _set_ttl($ttl = NULL) {
		if($ttl != NULL) {
			$this->data["ttl"] = $ttl;
		}
	}

	private function _set_collapse_key($collapse_key = NULL) {
		if($collapse_key != NULL) {
			$this->data["collapse_key"] = $collapse_key;
		}
	}

	public function send($registration_token, array $data){
		if(array_key_exists('data',$data)) $this->_set_message_data($data['data']);
		if(array_key_exists('notification',$data)) $this->_set_notification_data($data['notification']);
		if(array_key_exists('priority',$data)) $this->_set_priority($data['priority']);
		if(array_key_exists('ttl',$data)) $this->_set_ttl($data['ttl']);
		if(array_key_exists('collapse_key',$data)) $this->_set_collapse_key($data['collapse_key']);
		$this->_set_key($registration_token, (array_key_exists('recipient_key',$data) ? $data['recipient_key'] : "to"));

		if(!$this->app_key) throw new Exception("App key not set");

		$response = Curl::to("https://fcm.googleapis.com/fcm/send")
	    	->withHeader("Authorization:key={$this->app_key}")
		    ->withContentType("application/json")
	    	->withData($this->data)
	    	->asJson(TRUE)
	        ->post();

	    return $response;

	}

}
