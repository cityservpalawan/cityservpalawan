<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQueueForDisplayTableAddedTypeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('queue_for_display', function($table)
        {
            $table->string('type')->default('queue')->after('is_priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('queue_for_display', function($table)
        {
            $table->dropColumn(array('type'));
        });
    }
}
