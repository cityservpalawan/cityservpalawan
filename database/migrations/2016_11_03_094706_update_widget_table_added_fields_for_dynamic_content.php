<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWidgetTableAddedFieldsForDynamicContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('widget', function($table)
        {
            $table->integer('parent_id')->default(0)->after('id');
            $table->enum('is_parent',[1,0])->default(1)->after('code');
            $table->string('type',50)->nullable()->after('code');
            $table->string('display_screen',50)->nullable()->after('code');
            $table->text('content')->nullable()->after('code');
            $table->string('list_type',50)->nullable()->after('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('widget', function($table)
        {
            $table->dropColumn(array('parent_id','type','display_screen','content','is_parent'));
        });
    }
}
