<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizen_report', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('user_id')->default(0);
            $table->string('code',20);
            $table->string('type',50)->nullable();

            $table->text('content');
            $table->string('geo_lat',50)->nullable();
            $table->string('geo_long',50)->nullable();
            
            $table->text('directory')->nullable();
            $table->string('filename',150)->nullable();
            $table->text('path')->nullable();

            $table->string('status',50)->default("pending");
            $table->dateTime('posted_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('citizen_report');
    }
}
