<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password',60);

            $table->string('fname',50);
            $table->string('lname',50);
            $table->string('contact_number',100);
            $table->string('age',2);
            $table->enum('gender',['','male','female'])->default('');
            
            $table->bigInteger('fb_id')->nullable()->unsigned()->index();
            $table->string('access_token')->nullable();
            $table->string('validation_token',64)->nullable();

            $table->string('type',50)->default('user');
            $table->enum("is_lock",['yes','no'])->default('no');
            $table->timestamp('last_login')->nullable();

            $table->text('directory')->nullable();
            $table->string('filename',150)->nullable();
            $table->text('path')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('user');
    }
}
