<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTableAddedNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function($table)
        {
            $table->string('activation_code')->nullable();
            $table->enum('is_active',['yes','no'])->default('no');
            $table->string('account_number')->nullable();
            $table->string('barangay')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function($table)
        {
            $table->dropColumn(array('activation_code','is_active','account_number','barangay'));

        });
    }
}
