<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_log', function(Blueprint $table){
            $table->bigIncrements('id')->unsigned();
            $table->integer('user_id');
            $table->text('msg');
            $table->string('type',50);
            $table->integer('reference_id')->default(0);
            $table->string('title');
            $table->text('thumbnail')->nullable();
            $table->enum('is_read',['no','yes'])->default('no');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_log');
    }
}
