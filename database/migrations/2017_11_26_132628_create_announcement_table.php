<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('title',150);
            $table->text('slug');
            $table->string('excerpt');
            $table->text('content');

            $table->text('directory')->nullable();
            $table->string('filename',150)->nullable();
            $table->text('path')->nullable();
            
            $table->enum('status',["draft","published"])->default("draft");
            $table->enum('featured',[1,0])->default(0);
            $table->dateTime('posted_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('announcement');
    }
}
