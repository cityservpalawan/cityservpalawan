<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMomentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moment', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('user_id')->default(0);

            $table->text('content');
            $table->text('directory')->nullable();
            $table->string('filename',150)->nullable();
            $table->text('path')->nullable();

            $table->string('status',50)->default("pending");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('moment');
    }
}
