<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey', function(Blueprint $table){
            $table->bigIncrements('id')->unsigned();
            $table->integer('user_id')->default(0);
            $table->integer('citizen_request_id')->default(0);
            $table->string('tracking_number')->nullable();
            $table->integer('item_no');
            $table->text('answer')->nullable();
            $table->integer('equivalent_value')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey');
    }
}
