<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code');

            $table->string('title',150);
            $table->text('slug');
            $table->string('excerpt');
            $table->text('content');
            
            $table->enum('status',["draft","published"])->default("draft");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page');
    }
}
