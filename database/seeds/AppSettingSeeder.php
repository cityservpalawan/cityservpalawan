<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\AppSetting;

use Carbon\Carbon;
use Illuminate\Support\Str;

class AppSettingSeeder extends Seeder{
	public function run(){

	   AppSetting::truncate();
	   AppSetting::create([ 'type' => "text", 'title' => "Version Name", 'code' => "version_name", 'value' => "1.0.0.0", 'status' => "published" ]);
	   AppSetting::create([ 'type' => "text", 'title' => "Major Version", 'code' => "major_version", 'value' => "1.0", 'status' => "published" ]);
	   AppSetting::create([ 'type' => "text", 'title' => "Minor Version", 'code' => "minor_version", 'value' => "1.0", 'status' => "published" ]);
	   AppSetting::create([ 'type' => "text", 'title' => "Changelogs", 'code' => "changelogs", 'value' => "Initial Commit.", 'status' => "published" ]);
	}
}