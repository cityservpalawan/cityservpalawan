@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-stack2"></i> <span class="text-semibold">Widgets</span> - Create a new widget.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.widgets.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.widgets.index')}}"> Widgets</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Widget Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for this widget.</p>

				<div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}">
					<label for="type" class="control-label col-lg-2 text-right">Type <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("type", $types, old('type'), ['id' => "type", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('type'))
						<span class="help-block">{{$errors->first('type')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('display_type') ? 'has-error' : NULL}}">
					<label for="display_type" class="control-label col-lg-2 text-right">Type of Display</label>
					<div class="col-lg-9">
						{!!Form::select("display_type", $display_types, old('display_type'), ['id' => "display_type", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('display_type'))
						<span class="help-block">{{$errors->first('display_type')}}</span>
						@endif
						<span class="help-block">Ignore if the widget is <code>not child</code> of any parent widget</span>
					</div>
				</div>

				<div class="form-group {{$errors->first('title') ? 'has-error' : NULL}}">
					<label for="title" class="control-label col-lg-2 text-right">Title <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="title" id="title" placeholder="" maxlength="100" value="{{old('title')}}">
						@if($errors->first('title'))
						<span class="help-block">{{$errors->first('title')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('short_description') ? 'has-error' : NULL}}">
					<label for="short_description" class="control-label col-lg-2 text-right">Short Description</label>
					<div class="col-lg-9">
						<textarea name="short_description" id="short_description" cols="5" rows="5" class="form-control" placeholder="Type anything here...">{!!old('short_description')!!}</textarea>
						@if($errors->first('short_description'))
						<span class="help-block">{{$errors->first('short_description')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('display_screen') ? 'has-error' : NULL}}">
					<label for="display_screen" class="control-label col-lg-2 text-right">Show in screen <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("display_screen", $display_screens, old('display_screen'), ['id' => "display_screen", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('display_screen'))
						<span class="help-block">{{$errors->first('display_screen')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group pl-10">
					<div class="checkbox col-lg-offset-2 col-lg-5">
						<label>
							<input type="checkbox" name="is_parent" class="styled" value="1" {{old('is_parent') == "1" ? 'checked' : NULL}}>
							Is parent?
							<span class="help-block">A parent widget contains child widgets in a form of a <code>list</code>.</span>
						</label>
					</div>
				</div>

				<div class="form-group {{$errors->first('parent_id') ? 'has-error' : NULL}}">
					<label for="parent_id" class="control-label col-lg-2 text-right">Parent Widget </label>
					<div class="col-lg-9">
						{!!Form::select("parent_id", $parents, old('parent_id'), ['id' => "parent_id", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('parent_id'))
						<span class="help-block">{{$errors->first('parent_id')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
					<label class="control-label col-lg-2 text-right">Upload thumbnail</label>
					<div class="col-lg-9">
						<input type="file" name="file" class="file-styled-primary">
						@if($errors->first('file'))
						<span class="help-block">{{$errors->first('file')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('display_order') ? 'has-error' : NULL}}">
					<label for="display_order" class="control-label col-lg-2 text-right">Display Order <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="display_order" id="display_order" placeholder="" maxlength="100" value="{{old('display_order',0)}}">
						@if($errors->first('display_order'))
						<span class="help-block">{{$errors->first('display_order')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('status') ? 'has-error' : NULL}}">
					<label for="status" class="control-label col-lg-2 text-right">Status <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("status", $statuses, old('status'), ['id' => "status", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('status'))
						<span class="help-block">{{$errors->first('status')}}</span>
						@endif
					</div>
				</div>
				
			</div>
		</div>

		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Widget Content</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				<p class="content-group-lg">The content of your widget.</p>
				<textarea type="textbox" name="content" id="content" class="summernote">{!!old('content')!!}</textarea>
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.widgets.index')}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Summernote -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/summernote/summernote.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>


<script type="text/javascript">
	$(function(){

		$('#content').summernote({
	    	height: 400,
	    	onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
	    });

	     function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_KEY')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                	if(data.status == true){
                		$('#content').summernote('insertImage', data.image);
                	}
                }
            });
        }

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	});
</script>
@stop