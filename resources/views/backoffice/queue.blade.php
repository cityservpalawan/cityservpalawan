@extends("backoffice._template.queue")

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="category-header">
				<h4 class="content-group text-semibold">Select a Service <small class="display-block">Choose a service to display the queueing request</small></h4>
			</div>
			<div class="category-content">
				<div class="row">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-12">
								<button class="btn bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-people"></i> <span>Priority Lane</span></button>
							</div>
						</div>
						<br>
					</div>
					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-12">
								<button class="btn bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-people"></i> <span>Administrative</span></button>
							</div>
						</div>
						<br>
					</div>

					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-6">
								<button class="btn bg-pink btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-accessibility2"></i> <span>Social Service</span></button>		
							</div>
							<div class="col-md-6">
								<button class="btn bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-pulse2"></i> <span>Health Service</span></button>		
							</div>
						</div>
						<br>
					</div>
					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-6">
								<button class="btn bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-books"></i> <span>Scholarship Program</span></button>		
							</div>
							<div class="col-md-6">
								<button class="btn bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-balance"></i> <span>Legal Service</span></button>		
							</div>
						</div>
						<br>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">List of Request</h5>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="reload"></a></li>
	                	</ul>
                	</div>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<div class="panel-body">
					Table below will be the active queueing requests for the selected service.
				</div>

				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr class="bg-pink">
								<th width="5%">#</th>
								<th width="25%">Service</th>
								<th class="text-center">Date Requested</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach(range(1,8) as $index => $value)
							<tr>
								<td>{{$value}}</td>
								<td>
									<h6 class="media-heading"><strong>S-00{{$value}} </strong></h6>
									<small>Social Service</small>
								</td>
								<td class="text-center">12 Dec 2016 @ 10:10 AM</td>
								<td class="text-center">
									@if($index == 0)
										<button class="label label-success"><i class="icon-watch"></i> QUEUE</button>
									@endif

									@if($index == 1)
										<button class="label label-danger"><i class="icon-stop"></i> Stop</button>
									@endif

									@if($index == 3)
										<button class="label label-warning"><i class="icon-next"></i> SKIP</button>
									@endif

									@if($index == 2)
										<button class="label label-success"><i class="icon-play3"></i> Start</button>
									@endif

									@if($index == 4)
										<button class="label label-danger"><i class="icon-blocked"></i> Cancel</button>
									@endif

								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-scripts')
{{-- <script type="text/javascript" src="{{asset('backoffice/responsivevoice.js')}}"></script> --}}
<script src="http://code.responsivevoice.org/responsivevoice.js"></script>
<script type="text/javascript">
	$(function(){
		var voicelist = responsiveVoice.getVoices();
		$("button").on("click",function(){
			responsiveVoice.speak("Now Serving Social Service Division S,0,0,1","US English Male", {pitch: 1,volume : 1});
		});
	});
</script>
@stop