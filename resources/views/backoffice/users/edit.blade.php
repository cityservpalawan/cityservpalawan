@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-users"></i> <span class="text-semibold">Citizen</span> - Edit a citizen account.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.users.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
				<a href="{{route('backoffice.users.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.users.index')}}"> Citizen</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	<div class="tabbable">
		<ul class="nav nav-tabs nav-tabs-solid" style="background-color: #fff;">
			<li class="active"><a href="#basic-tab1" data-toggle="tab">User Details</a></li>
			{{--
			<li><a href="#basic-tab2" data-toggle="tab">MAC History</a></li>
			--}}
		</ul>

		<div class="tab-content">
			<div class="tab-pane active" id="basic-tab1">
				<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">User Details</h5>
							<div class="heading-elements">
								<ul class="icons-list">
				            		<li><a data-action="collapse"></a></li>
				            		<!-- <li><a data-action="reload"></a></li> -->
				            		<!-- <li><a data-action="close"></a></li> -->
				            	</ul>
				        	</div>
						</div>

						<div class="panel-body">
							
							<p class="content-group-lg">Below are the general information for this user.</p>

							<div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}">
								<label for="type" class="control-label col-lg-2 text-right">User Type <span class="text-danger"> *</span></label>
								<div class="col-lg-9">
									{!!Form::select("type", $types, old('type',$user->type), ['id' => "type", 'class' => "select select-no-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
									@if($errors->first('type'))
									<span class="help-block">{{$errors->first('type')}}</span>
									@endif
								</div>
							</div>
							
							<div class="form-group {{$errors->first('fname') ? 'has-error' : NULL}}">
								<label for="fname" class="control-label col-lg-2 text-right">First Name <span class="text-danger"> *</span></label>
								<div class="col-lg-9">
									<input class="form-control" type="text" name="fname" id="fname" placeholder="" maxlength="100" value="{{old('fname',$user->fname)}}">
									@if($errors->first('fname'))
									<span class="help-block">{{$errors->first('fname')}}</span>
									@endif
								</div>
							</div>

							<div class="form-group {{$errors->first('lname') ? 'has-error' : NULL}}">
								<label for="lname" class="control-label col-lg-2 text-right">Last Name <span class="text-danger"> *</span></label>
								<div class="col-lg-9">
									<input class="form-control" type="text" name="lname" id="lname" placeholder="" maxlength="100" value="{{old('lname',$user->lname)}}">
									@if($errors->first('lname'))
									<span class="help-block">{{$errors->first('lname')}}</span>
									@endif
								</div>
							</div>

							<div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
								<label for="email" class="control-label col-lg-2 text-right">Email <span class="text-danger"> *</span></label>
								<div class="col-lg-9">
									<input class="form-control" type="text" name="email" id="email" placeholder="" maxlength="100" value="{{old('email',$user->email)}}">
									@if($errors->first('email'))
									<span class="help-block">{{$errors->first('email')}}</span>
									@endif
								</div>
							</div>

							<div class="form-group {{$errors->first('contact_number') ? 'has-error' : NULL}}">
								<label for="contact_number" class="control-label col-lg-2 text-right">Contact # <span class="text-danger"> *</span></label>
								<div class="col-lg-9">
									<input class="form-control" type="text" name="contact_number" id="contact_number" placeholder="" maxlength="100" value="{{old('contact_number', $user->contact_number)}}">
									@if($errors->first('contact_number'))
									<span class="help-block">{{$errors->first('contact_number')}}</span>
									@endif
								</div>
							</div>

							<div class="form-group {{$errors->first('gender') ? 'has-error' : NULL}}">
								<label for="gender" class="control-label col-lg-2 text-right">Gender</label>
								<div class="col-lg-9">
									{!!Form::select("gender", $genders, old('gender', $user->gender), ['id' => "gender", 'class' => "select select-no-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
									@if($errors->first('gender'))
									<span class="help-block">{{$errors->first('gender')}}</span>
									@endif
								</div>
							</div>

							<div class="form-group {{$errors->first('birthdate') ? 'has-error' : NULL}}">
								<label for="birthdate" class="control-label col-lg-2 text-right">Birthdate </label>
								<div class="col-lg-9">
									<input type="text" id="birthdate" name="birthdate" class="dropup form-control daterange-single" placeholder="YYYY-MM-DD" value="{{old('birthdate', ($user->birthdate AND $user->birthdate!= "0000-00-00")?$user->birthdate:NULL)}}">
									@if($errors->first('birthdate'))
									<span class="help-block">{{$errors->first('birthdate')}}</span>
									@endif
								</div>
							</div>

							{{--
							<div class="form-group {{$errors->first('barangay') ? 'has-error' : NULL}}">
								<label for="barangay" class="control-label col-lg-2 text-right">Barangay </label>
								<div class="col-lg-9">
									{!!Form::select("barangay", $barangays, old('barangay', $user->barangay), ['id' => "barangay", 'class' => "select select-with-search col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
									@if($errors->first('barangay'))
									<span class="help-block">{{$errors->first('barangay')}}</span>
									@endif
								</div>
							</div>

							<div class="form-group {{$errors->first('account_number') ? 'has-error' : NULL}}">
								<label for="account_number" class="control-label col-lg-2 text-right">Account Number </label>
								<div class="col-lg-9">
									<input type="text" id="account_number" name="account_number" class="form-control" placeholder="" value="{{old('account_number', $user->account_number)}}">
									@if($errors->first('account_number'))
									<span class="help-block">{{$errors->first('account_number')}}</span>
									@endif
								</div>
							</div>
							--}}

							<div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
								<label class="control-label col-lg-2 text-right">Upload thumbnail</label>
								<div class="col-lg-9">
									<input type="file" name="file" class="file-styled-primary">
									@if($errors->first('file'))
									<span class="help-block">{{$errors->first('file')}}</span>
									@endif
								</div>
							</div>

							<div class="form-group">
								@if($user->filename)
								<label class="control-label col-lg-2 text-right">Current thumbnail</label>
								<div class="col-lg-9">
									<img src="{{asset($user->directory.'/resized/'.$user->filename)}}" class="img-responsive img-rounded" width="250" alt="">
								</div>
								@else
								<div class="col-lg-offset-2">
									<label for="" class="pl-10">No thumbnail yet.</label>
								</div>
								@endif
							</div>

							{{--
							@if($auth->type == "super_user")
							<div class="form-group pl-10">
								<div class="checkbox col-lg-offset-2 col-lg-5">
									<label>
										<input type="checkbox" name="is_active" class="styled" value="yes" {{old('is_active', $user->is_active) == "yes" ? 'checked' : NULL}}>
										Activate Account
										<span class="help-block">If checked, this account will be activated.</span>
									</label>
								</div>
							</div>
							@else
							<div class="form-group {{$errors->first('is_active') ? 'has-error' : NULL}}">
								<label for="is_active" class="control-label col-lg-2 text-right">Account Activated </label>
								<div class="col-lg-9">
									<input type="text" id="is_active" name="is_active" class="form-control" placeholder="" value="{{old('is_active', $user->is_active)}}" readonly="readonly">
									@if($errors->first('is_active'))
									<span class="help-block">{{$errors->first('is_active')}}</span>
									@endif
								</div>
							</div>
							@endif
							--}}

							<div class="form-group {{$errors->first('password') ? 'has-error' : NULL}}">
								<label for="password" class="control-label col-lg-2 text-right">New Password</label>
								<div class="col-lg-9">
									<input class="form-control" type="password" name="password" id="password" placeholder="" maxlength="100" value="{{old('password')}}">
									<span class="help-block">If you do not wish to change the password of this account, do not enter anything in this field and the field below.</span>
									@if($errors->first('password'))
									<span class="help-block">{{$errors->first('password')}}</span>
									@endif
								</div>
							</div>
							
							<div class="form-group {{$errors->first('password_confirmation') ? 'has-error' : NULL}}">
								<label for="password_confirmation" class="control-label col-lg-2 text-right">Confirm password</label>
								<div class="col-lg-9">
									<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="" maxlength="100" value="{{old('password_confirmation')}}">
									@if($errors->first('password_confirmation'))
									<span class="help-block">{{$errors->first('password_confirmation')}}</span>
									@endif
								</div>
							</div>
							
						</div>
					</div>

					<div class="content-group">
						<div class="text-left">
							<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
							&nbsp;
							<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.users.index')}}">Cancel</a>
						</div>
					</div>
				</form>
			</div>

			<div class="tab-pane" id="basic-tab2">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">MAC History</h5>
						<div class="heading-elements">
							<ul class="icons-list">
			            		<li><a data-action="collapse"></a></li>
			            		<!-- <li><a data-action="reload"></a></li> -->
			            		<!-- <li><a data-action="close"></a></li> -->
			            	</ul>
			        	</div>
					</div>

					<div class="panel-body">
						<p>Here are the list of <code>all MAC requests</code> made / tracked by this citizen.</strong></p>
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th class="text-center" width="50px">#</th>
										<th width="15%">Tracking #</th>
										<th width="20%">Request Info</th>
										<th class="text-center">Status</th>
										<th  width="15%" class="text-center">Date of Request</th>
										<th  width="15%" class="text-center">Appointment Date</th>
									</tr>
								</thead>
								<tbody>
								@forelse($requests as $index => $request)
									<tr>
										<td class="text-center">{{++$index}} </td>
										<td><strong>{{Str::upper($request->tracking_number?:"---")}}</strong></td>
										<td class="no-margin" >
											<p class="text-bold no-margin" data-popup="tooltip" title="{{$request->title}}" data-placement="bottom">{{Str::limit($request->title,25)}}</p>
											<div><small>by {{$request->name ? : "---"}}</small></div>
										</td>
										<td class="text-center">
											<small class="text-semibold">{{($request->tracker ? $request->tracker->num_completed : 0) ." / " . ($request->tracker ? $request->tracker->num_process : 0)}} Completed</small>

											<div class="progress">
												<div class="progress-bar  {{$request->tracker->percentage != "100" ? 'progress-bar-striped active' : NULL}} {{Helper::progress_color($request->tracker ? $request->tracker->percentage : 0)}}" style="width: {{$request->tracker ? $request->tracker->percentage : 0}}%">
													
												</div>
											</div>
										</td>
										<td class="text-center" title="{{$request->created_at}}">{{$request->date_format($request->created_at,'Y-m-d H:i')}}</td>
										<td class="text-center" title="{{$request->appointment_date}}">{{$request->date_format($request->tracker->appointment_schedule,'Y-m-d H:i')}}</td>
									</tr>
								@empty
								<tr>
									<td colspan="6" class="text-center"><i>No data available.</i></td>
								</tr>
								@endforelse	
								</tbody>
							</table>
						</div>
					</div>

					
				</div>
			</div>
		</div>
	</div>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- CKEditor -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>

<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('.select-no-search').select2({
			minimumResultsForSearch: Infinity
		});

		$('.select-with-search').select2();

	    $('#birthdate').daterangepicker({ 
			autoApply: true,
			autoUpdateInput: false,
			singleDatePicker: true,
			timePicker: false,
			showDropdowns: true,
			locale: {
				format: 'YYYY-MM-DD'
			}
		}).on('apply.daterangepicker', function (ev, picker){
			$(this).val(picker.startDate.format("YYYY-MM-DD"));
		});


	});
</script>
@stop