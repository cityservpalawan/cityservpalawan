@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-certificate"></i> <span class="text-semibold">Mayor's Action Center</span> - View transaction details of a MAC request.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.citizen_requests.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
				<a href="{{route('backoffice.citizen_requests.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.citizen_requests.index')}}"> Mayor's Action Center</a></li>
			<li class="active"># {{ $request->tracking_number ? : "---" }}</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		
		<div class="col-lg-9 col-md-9 col-sm-8">

			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">MAC Request Tracker</h6>
					<div class="heading-elements">
						<span class="heading-text"><i class="icon-paperplane position-left text-success"></i> Sent {{$request->time_passed($request->created_at)}}</span>
	            	</div>
				</div>

				<div class="panel-body">

					<div class="table-responsive table-framed">
						<table class="table" id="target-table">
							<thead>
								<tr>
									<th>Task</th>
									<th>Admin</th>
									<th>Duration</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if( $request->tracker AND ( $request->tracker->info->count() > 1 ) )
									@foreach($request->tracker->info as $index => $process)
									<tr>
										<td><p class="text-semibold no-margin">{{$process->process_title}}</p></td>
										<td>{{$process->admin ? "{$process->admin->fname} {$process->admin->lname}" : "?"}}</td>
										<td>{!!$process->duration != NULL ? "<i class='icon-alarm'></i> {$process->duration} mins" : "?"!!}</td>
										<td class="text-center">{!!$process->status_badge('status')!!}</td>
										<td class="text-center">
											@if($process->status == "done")
												<span class="icon-checkmark4 text-success"></span>
											@elseif($process->status == "rejected")
												<span class="icon-cross2 text-danger"></span>
											@elseif($process->status == "for_revalidation")
												<div class="btn-group">
							                    	<button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">action <span class="caret"></span></button>
							                    	<ul class="dropdown-menu dropdown-menu-right">
														<li><a href="{{route('backoffice.citizen_requests.tracker_edit',[$process->id])}}">Revalidate</a></li>
														<li class="divider"></li>
														<li><a class="action-quick-update" data-url="{{route('backoffice.citizen_requests.tracker_quick_update',[$process->id])}}" data-status="done">Quick Accept</a></li>
														<li><a href="#" class="action-quick-update" data-url="{{route('backoffice.citizen_requests.tracker_quick_update',[$process->id])}}" data-status="rejected">Quick Reject</a></li>
													</ul>
												</div>
											@else
												@if($index == 0 OR ($processes->where('id', $process->id - 1)->first() AND in_array($processes->where('id', $process->id - 1)->first()->status, ['for_revalidation','done'])))
												<div class="btn-group">
							                    	<button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">action <span class="caret"></span></button>
							                    	<ul class="dropdown-menu dropdown-menu-right">
														<li><a href="{{route('backoffice.citizen_requests.tracker_edit',[$process->id])}}">Update</a></li>
														<li class="divider"></li>
														<li><a class="action-quick-update" data-url="{{route('backoffice.citizen_requests.tracker_quick_update',[$process->id])}}" data-status="done">Quick Accept</a></li>
														<li><a href="#" class="action-quick-update" data-url="{{route('backoffice.citizen_requests.tracker_quick_update',[$process->id])}}" data-status="rejected">Quick Reject</a></li>
													</ul>
												</div>
												@else
													<span class="fa fa-ellipsis-h text-slate-600"></span>
												@endif
											@endif
										</td>
									</tr>
									@endforeach
								@else
								<tr>
									<td class="text-center" colspan="5">Unable to fetch tracker.</td>
								</tr>
								@endif
							</tbody>
						</table>
					</div>					
				</div>
			</div>


			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Request Logs</h6>
					<div class="heading-elements">
						{{-- <span class="heading-text"><i class="icon-paperplane position-left text-success"></i> Sent {{$request->time_passed($request->created_at)}}</span> --}}
	            	</div>
				</div>

				<div class="panel-body">
					<div class="table-responsive table-framed">
						<table class="table">
							<thead>
								<tr>
									<th>Content</th>
									<th>Employee</th>
									<th>Time</th>
								</tr>
							</thead>
							<tbody>
								@foreach($request->logs as $log)
								<tr>
									<td><p class="no-margin text-semibold">{{$log->remarks}}</p></td>
									<td>{{$log->author ? "{$log->author->fname} {$log->author->lname}" . ($log->author->type == "user" ? " (Citizen)": NULL)  : "?"}}</td>
									<td>{{$log->time_passed($log->created_at)}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

		<div class="col-lg-3 col-md-3 col-sm-4">

			<div class="thumbnail">
				<div class="caption text-center">
					<h5 class="text-semibold no-margin">{{$request->title}}</h5>
					<h6 class="text-light text-muted no-margin">{{$request->code? : "---"}} / {{$request->tracking_number? : "---"}}</h6>
					<hr>
					<small class="">Requested Last : <strong>{{$request->date_format($request->created_at,'Y-m-d H:i')}}</strong></small><br>
					<small class="">Total Duration : <strong>{{Helper::mins_to_time($request->tracker ? $request->tracker->total_duration : 0 )}}</strong></small><br>
					<small>Released Amount : <strong>PHP {{$request->tracker ? $request->tracker->release_amount : "---"}}</strong></small>
				</div>
			</div>
			
			<div class="thumbnail">
				<div class="caption text-center">
					<small class="text-semibold">{{($request->tracker ? $request->tracker->num_completed : 0) ." / " . ($request->tracker ? $request->tracker->num_process : 0)}} Completed</small>
					<div class="progress">
						<div class="progress-bar  {{$request->tracker->percentage != "100" ? 'progress-bar-striped active' : NULL}} {{Helper::progress_color($request->tracker ? $request->tracker->percentage : 0)}}" style="width: {{$request->tracker ? $request->tracker->percentage : 0}}%">
						</div>
					</div>
				</div>
			</div>

			<div class="thumbnail pb-20">
				{{-- <h3 class="text-light text-center">{{$request->code}} <small class="display-block">{{"'{$request->title}'"}}</small></h3> --}}
				<div class="caption text-center">
					<h6 class="text-semibold no-margin"><span class="text-muted">Client : </span> {{$request->name}}</h6>
					@if($request->email)<small class="text-muted"><i class="icon-envelop2"></i> {{$request->email}}</small><br>@endif
					@if($request->contact_number)<small class="text-muted"><i class="icon-mobile"></i> {{$request->contact_number}}</small><br>@endif
				</div>
			</div>

			<div class="thumbnail pb-20">
				<div class="caption text-center">
					<h6 class="text-semibold no-margin">Beneficiary Details</h6>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Name</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_name ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Age</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_age ?: "---"}} yrs. old</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Birthdate</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_birthdate ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Birthplace</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_birthplace ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Gender</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_gender ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Civil Status</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_civil_status ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Religion</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_religion ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Address</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_address ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Education</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_education ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Occupation</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_occupation ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Income</div>
						<div class="col-xs-8 text-left">PHP {{$request->beneficiary_income ?: "---"}}</div>
					</div>
					<div class="row details">
						<div class="col-xs-4 text-muted text-right">Relationship to client</div>
						<div class="col-xs-8 text-left">{{$request->beneficiary_rel_to_client ?: "---"}}</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')

@stop
@section('page-styles')
<style type="text/css">
	.details {
		border-bottom: 1px solid #eeeded;
		padding-bottom: 5px;
		padding-top: 5px;
	}
</style>
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- CKEditor -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>

<!-- Fancybox -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/media/fancybox.min.js')}}"></script>

<!-- Gallery -->
<script type="text/javascript" src="{{asset('backoffice/js/pages/gallery.js')}}"></script>

<!-- Sweetalert -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/notifications/sweet_alert.min.js')}}"></script>

<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('.daterange-single').daterangepicker({ 
			singleDatePicker: true,
			timePicker: true,
			timePickerIncrement: 1,
			locale: {
				format: 'YYYY-MM-DD h:mm A'
			}
		});

	    $('#target-table').find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');

	    $('#target-table').delegate('.action-quick-update', "click", function(){
	    	
	    	var url =  $(this).data('url');
	    	var status =  $(this).data('status');

	    	if(status == "done") {

	    		var is_last = $(this).closest("tr").is(":last-child");

	    		if(is_last) {

	    			swal({
			    		title: "Quick Accept",
			    		text: 'Update the chosen task to "Done"',
			    		type: "input",
			    		html: true,
			    		animation: "slide-from-top",
			    		inputPlaceholder: "Please indicate released amount",
			    		showCancelButton: true,
			    		closeOnConfirm: true,
			    		confirmButtonColor: "#4caf50",
			    		confirmButtonText : "Accept",
			    		showLoaderOnConfirm: true
			    	},
			    	function(release_amount){
			    		if (release_amount === false) return false;

			    		if (release_amount === "") {
			    			swal.showInputError("You need to enter amount!");
			    			return false;
			    		}

			    		if ($.isNumeric(release_amount) == false) {
			    			swal.showInputError("You need to enter a valid amount!");
			    			return false;
			    		}

			    		window.location.href = url + "?status=" + status + "&release_amount=" + release_amount;
					});

	    		} else {

	    			swal({
			    		title: "Quick Accept",
			    		text: 'Update the chosen task to "Done"',
			    		html: true,
			    		animation: "slide-from-top",
			    		showCancelButton: true,
			    		closeOnConfirm: true,
			    		confirmButtonColor: "#4caf50",
			    		confirmButtonText : "Accept",
			    		showLoaderOnConfirm: true
			    	},
			    	function(confirm){
			    		if (confirm === false) return false;

			    		window.location.href = url + "?status=" + status;
					});
	    		}

	    	} else {

	    		swal({
		    		title: "Quick Reject",
		    		text: 'Update the chosen task to "Rejected"',
		    		type: "input",
		    		html: true,
		    		animation: "slide-from-top",
		    		inputPlaceholder: "Please indicate remarks",
		    		showCancelButton: true,
		    		closeOnConfirm: true,
		    		confirmButtonColor: "#F44336",
		    		confirmButtonText : "Reject",
		    		showLoaderOnConfirm: true
		    	},
		    	function(remarks){
		    		if (remarks === false) return false;

		    		if (remarks === "") {
		    			swal.showInputError("You need to enter remarks!");
		    			return false;
		    		}

		    		window.location.href = url + "?status=" + status + "&remarks=" + remarks;
				});
	    	}

	    	
		});

	});
</script>
@stop