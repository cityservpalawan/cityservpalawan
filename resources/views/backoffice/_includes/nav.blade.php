	<li class="{{Helper::is_active($routes['dashboard'])}}">
		<a href="{{route('backoffice.dashboard')}}"><i class="icon-home4"></i> <span>Dashboard</span></a>
	</li>

	@if($auth->type == "super_user" OR $auth->type == "admin" OR $auth->type == "receptionist" OR $auth->type == "teller" OR $auth->type == "tv" )
	<li class="navigation-header"><span>Queue</span> <i class="icon-menu" title="Queue"></i></li>
	@if($auth->type == "super_user" OR $auth->type == "admin" OR $auth->type == "tv")
	<li class="">
		<a href="{{route('backoffice.queue.tv')}}"><i class="icon-tv"></i> <span>Calling Display</span></a>
	</li>
	@endif
	@if($auth->type == "super_user" OR $auth->type == "admin" OR $auth->type == "teller")
	<li class="">
		<a href="{{route('backoffice.queue.queue')}}"><i class="icon-tv"></i> <span>User Screen</span></a>
	</li>
	@endif
	@if($auth->type == "super_user" OR $auth->type == "admin" OR $auth->type == "receptionist")
	<li class="">
		<a href="{{route('backoffice.queue.index')}}"><i class="icon-tv"></i> <span>Receptionist Screen</span></a>
	</li>
	@endif
	@endif

	@if($auth->type == "super_user" OR $auth->type == "admin" OR $auth->type == "teller")
	<li class="navigation-header"><span>MAC</span> <i class="icon-menu" title="MAC"></i></li>

	<li class="{{Helper::is_active($routes['citizen_requests'],'active')}}">
		<a href="#"><i class="icon-certificate"></i> <span>Mayor's Action Center</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['citizen_requests'][0]])}}"><a href="{{route('backoffice.citizen_requests.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['citizen_requests'][1]])}}"><a href="{{route('backoffice.citizen_requests.create')}}">Create New</a></li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['schools'],'active')}}">
		<a href="#"><i class="icon-office"></i> <span>Schools</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['schools'][0]])}}"><a href="{{route('backoffice.schools.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['schools'][1]])}}"><a href="{{route('backoffice.schools.create')}}">Create New</a></li>
		</ul>
	</li>

{{--
	<li class="navigation-header"><span>Reporting</span> <i class="icon-menu" title="MAC"></i></li>

	<li class="{{Helper::is_active($routes['reports'],'active')}}">
		<a href="#"><i class="icon-statistics"></i> <span>Reports</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['reports'][0]])}}"><a href="{{route('backoffice.reports.index')}}">Generate</a></li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['report_footers'],'active')}}">
		<a href="#"><i class="icon-stack"></i> <span>Footers</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['report_footers'][0]])}}"><a href="{{route('backoffice.report_footers.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['report_footers'][1]])}}"><a href="{{route('backoffice.report_footers.create')}}">Create New</a></li>
		</ul>
	</li>
--}}	
	@endif
 	
	@if($auth->type == "super_user" OR $auth->type == "admin" OR $auth->type == "receptionist")
	<li class="navigation-header"><span>User Management</span> <i class="icon-menu" title="User Management"></i></li>
	
	<li class="{{Helper::is_active($routes['users'],'active')}}">
		<a href="#"><i class="icon-users"></i> <span>Citizen Accounts</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['users'][0]])}}"><a href="{{route('backoffice.users.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['users'][1]])}}"><a href="{{route('backoffice.users.create')}}">Create New</a></li>
		</ul>
	</li>

	@if($auth->type == "super_user")
	<li class="{{Helper::is_active($routes['admins'],'active')}}">
		<a href="#"><i class="icon-users"></i> <span>Administrator Accounts</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['admins'][0]])}}"><a href="{{route('backoffice.admins.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['admins'][1]])}}"><a href="{{route('backoffice.admins.create')}}">Create New</a></li>
		</ul>
	</li>
	@endif
	@endif

	@if($auth->type == "super_user" OR $auth->type == "admin")
	<li class="navigation-header"><span>Mobile Application</span> <i class="icon-menu" title="Mobile Application"></i></li>

	<li>
		<a href="#"><i class="icon-mobile"></i> <span>Content Management</span></a>
		<ul>
			<li class="{{Helper::is_active($routes['app_settings'],'active')}}">
				<a href="#"><i class="icon-cog"></i> <span>App Settings</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['app_settings'][0]])}}"><a href="{{route('backoffice.app_settings.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['app_settings'][1]])}}"><a href="{{route('backoffice.app_settings.create')}}">Create New</a></li>
				</ul>
			</li>
			
			<li class="{{Helper::is_active($routes['widgets'],'active')}}">
				<a href="#"><i class="icon-stack2"></i> <span>Widgets</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['widgets'][0]])}}"><a href="{{route('backoffice.widgets.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['widgets'][1]])}}"><a href="{{route('backoffice.widgets.create')}}">Create New</a></li>
				</ul>
			</li>

			<li class="{{Helper::is_active($routes['articles'],'active')}}">
				<a href="#"><i class="icon-newspaper"></i> <span>Articles</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['articles'][0]])}}"><a href="{{route('backoffice.articles.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['articles'][1]])}}"><a href="{{route('backoffice.articles.create')}}">Create New</a></li>
				</ul>
			</li>

			<li class="{{Helper::is_active($routes['announcements'],'open active')}}">
				<a href="#"><i class="icon-megaphone"></i> <span>Announcements</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['announcements'][0]])}}"><a href="{{route('backoffice.announcements.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['announcements'][1]])}}"><a href="{{route('backoffice.announcements.create')}}">Create New</a></li>
				</ul>
			</li>

			<li class="{{Helper::is_active($routes['events'],'active')}}">
				<a href="#"><i class="icon-calendar"></i> <span>Events</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['events'][0]])}}"><a href="{{route('backoffice.events.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['events'][1]])}}"><a href="{{route('backoffice.events.create')}}">Create New</a></li>
				</ul>
			</li>
			
			<li class="{{Helper::is_active($routes['directories'],'active')}}">
				<a href="#"><i class="icon-cube"></i> <span>Directories</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['directories'][0]])}}"><a href="{{route('backoffice.directories.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['directories'][1]])}}"><a href="{{route('backoffice.directories.create')}}">Create New</a></li>
				</ul>
			</li>

			<li class="{{Helper::is_active($routes['establishments'],'active')}}">
				<a href="#"><i class="icon-office"></i> <span>Establishments</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['establishments'][0]])}}"><a href="{{route('backoffice.establishments.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['establishments'][1]])}}"><a href="{{route('backoffice.establishments.create')}}">Create New</a></li>
				</ul>
			</li>

			<li class="{{Helper::is_active($routes['services'],'active')}}">
				<a href="#"><i class="icon-wrench"></i> <span>Services</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['services'][0]])}}"><a href="{{route('backoffice.services.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['services'][1]])}}"><a href="{{route('backoffice.services.create')}}">Create New</a></li>
				</ul>
			</li>

			<li class="{{Helper::is_active($routes['subservices'],'active')}}">
				<a href="#"><i class="icon-wrench"></i> <span>Sub-services</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['subservices'][0]])}}"><a href="{{route('backoffice.subservices.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['subservices'][1]])}}"><a href="{{route('backoffice.subservices.create')}}">Create New</a></li>
				</ul>
			</li>

			<li class="{{Helper::is_active($routes['emergencies'],'open active')}}">
				<a href="#"><i class="icon-cube"></i> <span>Emergencies</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['emergencies'][0]])}}"><a href="{{route('backoffice.emergencies.index')}}">Record Data</a></li>
					<li class="{{Helper::is_active([$routes['emergencies'][1]])}}"><a href="{{route('backoffice.emergencies.create')}}">Create New</a></li>
				</ul>
			</li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['moments'],'active')}}">
		<a href="#"><i class="icon-camera"></i> <span>Moments</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['moments'][0]])}}"><a href="{{route('backoffice.moments.index')}}">Record Data</a></li>
		</ul>
	</li>
	@endif

	@if($auth->type == "super_user")
	<li class="{{Helper::is_active($routes['polls'],'active')}}">
		<a href="#"><i class="icon-certificate"></i> <span>Polls</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['polls'][0]])}}"><a href="{{route('backoffice.polls.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['polls'][1]])}}"><a href="{{route('backoffice.polls.create')}}">Create New</a></li>
		</ul>
	</li>
	@endif



	@if($auth->type == "super_user")
	<li class="navigation-header"><span>Be Prepared</span> <i class="icon-menu" title="Be Prepared"></i></li>

	<li class="{{Helper::is_active($routes['citizen_reports'],'open active')}}">
		<a href="#"><i class="icon-camera"></i> <span>Citizen Reports</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['citizen_reports'][0]])}}"><a href="{{route('backoffice.citizen_reports.index')}}">Record Data</a></li>
		</ul>
	</li>
	@endif

<!--	@if($auth->type == "super_user")
	<li class="navigation-header"><span>Requests</span> <i class="icon-menu" title="Requests"></i></li>

	<li class="{{Helper::is_active($routes['death_certificate_requests'],'open active')}}">
		<a href="#"><i class="icon-certificate"></i> <span>Death Certificates</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['death_certificate_requests'][0]])}}"><a href="{{route('backoffice.death_certificate_requests.index')}}">Record Data</a></li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['marriage_certificate_requests'],'open active')}}">
		<a href="#"><i class="icon-certificate"></i> <span>Marriage Certificates</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['marriage_certificate_requests'][0]])}}"><a href="{{route('backoffice.marriage_certificate_requests.index')}}">Record Data</a></li>
		</ul>
	</li>

	<li>
		<a href="#"><i class="icon-certificate"></i> <span>Adoption Certificates</span></a>
		<ul>
			<li class="{{Helper::is_active($routes['prior_adoption_requests'],'active')}}">
				<a href="#"><i class="icon-pen"></i> <span>Prior to Adoption</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['prior_adoption_requests'][0]])}}"><a href="{{route('backoffice.prior_adoption_requests.index')}}">Record Data</a></li>
				</ul>
			</li>

			<li class="{{Helper::is_active($routes['after_adoption_requests'],'active')}}">
				<a href="#"><i class="icon-pen"></i> <span>After Adoption</span></a>
				<ul>
					<li class="{{Helper::is_active([$routes['after_adoption_requests'][0]])}}"><a href="{{route('backoffice.after_adoption_requests.index')}}">Record Data</a></li>
				</ul>
			</li>
		</ul>
	</li>-->
	@endif
