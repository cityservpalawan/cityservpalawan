<!-- Main navbar -->
<div class="navbar navbar-inverse bg-slate-800">
	<div class="navbar-header">
	<a class="navbar-brand" href="{{route('backoffice.dashboard')}}"><span class="text-black">CITYSERV PALAWAN <i class="icon-cube4"></i> CPANEL</span></a>

		<ul class="nav navbar-nav visible-xs-block">
			<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
		</ul>
	</div>

	<div class="navbar-collapse collapse" id="navbar-mobile">
		<ul class="nav navbar-nav">
			<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
		</ul>

		<div class="navbar-right">
			<p class="navbar-text">{{Helper::greet()}}, {{$auth->fname}}</p>
			<p class="navbar-text"><span class="label bg-green-600">Online</span></p>
		</div>
	</div>
</div>
	<!-- /main navbar -->