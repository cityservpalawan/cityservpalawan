@extends("backoffice._template.queue")

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="category-header">
				<h4 class="content-group text-semibold">Select a Service <small class="display-block">Choose a service to display the queueing request</small></h4>
			</div>
			<div class="category-content">
				<div class="row">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-12">
								<button data-service_type="priority_lane" class="btn btn-service bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-people"></i> <span>Priority Lane</span></button>
							</div>
						</div>
						<br>
					</div>
					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-12">
								<button data-service_type="administrative" class="btn btn-service bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-people"></i> <span>Administrative</span></button>
							</div>
						</div>
						<br>
					</div>

					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-6">
								<button data-service_type="social_service" class="btn btn-service bg-pink btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-accessibility2"></i> <span>Social Service</span></button>
							</div>
							<div class="col-md-6">
								<button data-service_type="health_service" class="btn btn-service bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-pulse2"></i> <span>Health Service</span></button>
							</div>
						</div>
						<br>
					</div>
					<div class="col-xs-12">
						<div class="row">
							<div class="col-md-6">
								<button data-service_type="scholarship_program" class="btn btn-service bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-books"></i> <span>Scholarship Program</span></button>
							</div>
							<div class="col-md-6">
								<button data-service_type="legal_service" class="btn btn-service bg-default btn-block btn-float btn-float-lg legitRipple" type="button"><i class="icon-balance"></i> <span>Legal Service</span></button>
							</div>
						</div>
						<br>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">List of Request</h5>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="reload"></a></li>
	                	</ul>
                	</div>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<div class="panel-body">
					Table below will be the active queueing requests for the selected service.
				</div>

				<div class="table-responsive">
					<table class="table"  id="queue_table" data-service_type="social_service">
						<thead>
							<tr class="bg-pink">
								<th width="5%">#</th>
								<th width="25%">Service</th>
								<th class="text-center">Date Requested</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($queues as $index => $queue)
							<tr data-queue_id= "{{$queue->id}}" data-source="{{$queue->source}}">
								<td>{{++$index}}</td>
								<td>
									<div class="media-heading">
										<strong style="font-size: 18px;">{{$queue->queue_no}}</strong> <span><i>- {{$queue->name}}</i></span>
									</div>
									<span>{{Helper::get_service_display($queue->service_type)}}</span>
									@if($queue->is_priority == "yes")
									<small class="label label-rounded border-danger text-danger-600">PRIORITY</small>
									@endif
								</td>
								<td class="text-center">{{$queue->date_format($queue->created_at)}}</td>
								<td class="text-center">
									@if($queue->status == "pending" OR $queue->status == "for_display")
									<button class="label label-success btn-action" data-action="queue"><i class="icon-watch"></i> QUEUE</button>
									<button class="label label-success bg-danger btn-action" data-action="start"><i class="icon-play3"></i> Start</button>
									<button class="label label-warning btn-action" data-action="cancel"><i class="icon-blocked"></i> Cancel</button>
									@endif
									@if($queue->status == "on_going")
									<button class="label label-danger btn-action" data-action="stop"><i class="icon-stop"></i> Stop</button>
									
									
									@if(in_array($queue->service_type, ["social_service","scholarship_program"]) AND $queue->is_transfer == "no" )
										@if($queue->source != "mobile")
										<button class="label label-success btn-action" data-action="transfer"><i class="icon-share2"></i> Transfer</button>
										@endif
									@endif
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript" src="{{asset('backoffice/js/plugins/notifications/sweet_alert.min.js')}}"></script>
<script src="http://code.responsivevoice.org/responsivevoice.js"></script>
<script type="text/javascript">
	var _master_token = "{{env('APP_KEY')}}";
	$(function(){
		$("#queue_table").delegate(".btn-action","click",function(){
				var _btn = $(this);
				var _col = _btn.parents("td");
				var _tr = _btn.parents("tr");
				var _action = _btn.data("action");
				_btn.prop("disabled",true);
				// console.log(_action);
				switch(_action){
					case 'queue':
							_col.append($('<span class="badge badge-info">Calling Attention</span>').fadeIn(500))
							console.log(_tr.data("queue_id"))
							$.ajax("{{URL::to('api/queue/flash.json')}}", {
								type: 'POST',
								data : { api_token : _master_token,queue_id : _tr.data("queue_id")},
								success: function(data) {
									var result = data.data;
									console.log(result);
									_btn.prop("disabled",false)
								},
								error : function(jqXHR,textStatus,thrownError){
										// alert("Error")
										_btn.prop("disabled",false)
								},
								complete : function(){
										_col.find("span.badge").fadeOut(500,function(){
												$(this).remove();
										})
								}
							});
					break;
					case 'start':
						_col.append($('<span class="badge badge-info">Request is being served.</span>').fadeIn(500,function(){
							$.ajax("{{URL::to('api/queue/start.json')}}", {
								type: 'POST',
								data : { api_token : _master_token,queue_id : _tr.data("queue_id")},
								success: function(data) {
									var result = data.data;
									var _btns = _tr.find("button");
									var _td = _btns.parents("td");

									_btns.fadeOut(200,function(){
											$(this).remove();

									});

									var queue_table = $("#queue_table");
									var tbody = queue_table.children("tbody");
									var type = queue_table.attr("data-service_type")

									_td.append('<button class="label label-danger btn-action" data-action="stop"><i class="icon-stop"></i> Stop</button>');
									if(_tr.data("source") != "mobile" && ["social_service","scholarship_program","scholarship"].indexOf(type) != -1){
										_td.append('<button class="label label-success btn-action" data-action="transfer"><i class="icon-share2"></i> Transfer</button>');
									}
									

									
								},
								error : function(jqXHR,textStatus,thrownError){
										// alert("Error")
										_btn.prop("disabled",false)
								},
								complete : function(){
										_col.find("span.badge").fadeOut(500,function(){
												$(this).remove();
										})
								}
							});
						}))
						
					break;
					case 'cancel':
						$.ajax("{{URL::to('api/queue/cancel.json')}}", {
							type: 'POST',
							data : { api_token : _master_token,queue_id : _tr.data("queue_id")},
							success: function(data) {
								var result = data.data;
								
								_tr.fadeOut(300,function(){
									$(this).remove();
								})
							},
							error : function(jqXHR,textStatus,thrownError){
									// alert("Error")
									_btn.prop("disabled",false)
							},
							complete : function(){
									_col.find("span.badge").fadeOut(500,function(){
											$(this).remove();
									})
							}
						});
					break;
					case 'transfer':

							$("body").delegate(".btn-request","click",function(e){
								var _index = $(this).index();
								var _sibling = $(this).parent().siblings().children('a');
								_sibling.removeClass("btn-success");
								$(this).addClass("btn-success");

								return false;
							});

							var active_service_type = $("#queue_table").attr("data-service_type");
							var subcategory = "scholarship";

							if(active_service_type == "social_service"){
								swal({
								  title: "Create a citizen request ",
								  text: '<div class="btn-group"  aria-label="Basic example">'+
								    '<span class="col-xs-6"><a href="#" class="btn btn-request btn-xs btn-success" data-value="burial">Burial Assistance</a></span>'+
								    '<span class="col-xs-6"><a href="#" class="btn btn-request btn-xs" data-value="medical">Medical Assistance</a></span>'+
								  '</div>',
								  
								  html: true,
								  animation: "slide-from-top",
								  inputPlaceholder: "Please indicate citizen name",
							      showCancelButton: true,
							      closeOnConfirm: false,
							      confirmButtonColor: "#4caf50",
							      confirmButtonText : "Submit request",
							      showLoaderOnConfirm: true
								},
								function(isConfirm){
									
									if(isConfirm){
										var subcategory = $(".btn-request.btn-success").data("value");

										$.ajax("{{URL::to('api/queue/transfer.json')}}", {
											type: 'POST',
											data : { api_token : _master_token,queue_id : _tr.data("queue_id"), category : active_service_type, subcategory : subcategory },
											success: function(data) {

												var result = data.data;
												
												_btn.fadeOut(200,function(){
													$(this).remove();
												});

										  		swal({
										  			title : "Success!",
										  			type : "success",
										  			text : data.msg,
										  			timer: 2000,
										  			showConfirmButton: true
										  		})


											},
											error : function(jqXHR,textStatus,thrownError){
													_btn.prop("disabled",false)
											}
										});

									}else{
										_btn.prop("disabled",false)
									}
								});
							}else{

								$.ajax("{{URL::to('api/queue/transfer.json')}}", {
									type: 'POST',
									data : { api_token : _master_token,queue_id : _tr.data("queue_id"), category : active_service_type, subcategory : subcategory },
									success: function(data) {
										var result = data.data;
										
										_btn.fadeOut(200,function(){
											$(this).remove();
										});

										swal({
											title : "Success!",
											type : "success",
											text : data.msg,
											timer: 2000,
											showConfirmButton: true
										})
									},
									error : function(jqXHR,textStatus,thrownError){
											_btn.prop("disabled",false)
									}
								});
							}

							

							
						
					break;
					case 'stop':
						$.ajax("{{URL::to('api/queue/stop.json')}}", {
							type: 'POST',
							data : { api_token : _master_token,queue_id : _tr.data("queue_id")},
							success: function(data) {
								var result = data.data;
								
								_tr.fadeOut(300,function(){
									$(this).remove();
								})
							},
							error : function(jqXHR,textStatus,thrownError){
									// alert("Error")
									_btn.prop("disabled",false)
							},
							complete : function(){
									_col.find("span.badge").fadeOut(500,function(){
											$(this).remove();
									})
							}
						});
					break;
				}

		});

		$(".btn-service").on("click",function(){
				var _btn = $(this);

				if(_btn.hasClass("bg-default")){
					var _type = _btn.data("service_type")
					swal({
					    title: "Change Service",
					    text: "to: "+$(this).get_service(_type,"title"),
					    type: "warning",
					    showCancelButton: true,
					    closeOnConfirm: false,
					    confirmButtonColor: "#2196F3",
					    confirmButtonText : "Proceed and Reload",
					    showLoaderOnConfirm: true,
					},
					function() {
							// clearInterval(get_data);
							// clearInterval(check_process);


							_siblings = $(".btn-service")
							_siblings.removeClass();

							_siblings.addClass(_btn.get_style(""))
							_btn.removeClass();
							_btn.addClass(_btn.get_style(_type));

							// swal.close();
							var queue_table = $("#queue_table");
							var tbody = queue_table.children("tbody");
							queue_table.attr("data-service_type",_type);

							var thead = queue_table.find("thead").children("tr");
							thead.removeClass().addClass(_btn.get_style(_type,"table"));
							var type = _type
							tbody.fadeOut(500,function(){
								$(this).empty();
								$(this).fadeIn(100);
								$.ajax("{{URL::to('api/queue/service-data.json')}}", {
	 								type: 'POST',
	 								data : { api_token : _master_token,_type : type},
	 								async : true,
	 								success: function(data) {
	 									var result = data.data;
	 									if(result.length > 0){

	 										$.each(result,function(index,queue){

	 											var priority_content = "";
	 											if(queue.is_priority == "yes"){
	 												priority_content = '<small class="label label-rounded border-danger text-danger-600">PRIORITY</small>';
	 											}

	 											if(tbody.find('tr[data-queue_id="'+queue.id+'"]').length == 0){
	 												tbody.append($('<tr data-queue_id= "'+queue.id+'" data-source="'+queue.source+'"> <td>'+(eval(queue_table.find("tbody tr").length)+1)+'</td>'+'<td><div class="media-heading">'+ '<strong style="font-size: 18px;">'+queue.queue_no+'</strong> <span><i>- '+queue.name+'</i></span>'+ '</div>'+ '<span>'+queue.for_display+'</span> '+ priority_content+ '</div>'+'  </td> <td class="text-center">'+queue.created_at+'</td> <td class="text-center"><button class="label label-success btn-action" data-action="queue"><i class="icon-watch"></i> QUEUE</button> <button class="label label-success bg-danger btn-action" data-action="start"><i class="icon-play3"></i> Start</button> <button class="label label-warning btn-action" data-action="cancel"><i class="icon-blocked"></i> Cancel</button></td> </tr>').fadeIn(500))
	 											}
	 										})
	 									}
									},
									error : function(jqXHR,textStatus,thrownError){
											// alert("Error")
									}
								});
								swal.close();
							});

							// setInterval(get_data,4000);
							// setInterval(check_process, 4000);


					});
				}
		});

		

		setInterval(get_data, 4000);
				function get_data(){
						var queue_table = $("#queue_table");
						var tbody = queue_table.children("tbody");
						var type = queue_table.attr("data-service_type")
						var ids = "";

						$.each(tbody.find("tr"),function(index,row){
								// console.log();
								ids+=$(this).attr("data-queue_id")+",";
						}).promise().done(function(){
							 $.ajax("{{URL::to('api/queue/service-data.json')}}", {
									type: 'POST',
									data : { api_token : _master_token,_type : type,_ids : ids},
									dataType: "json",
									async: true,
									success: function(data) {
										var result = data.data;
										console.log(data)

										if(result.length > 0){
											$.each(result,function(index,queue){
												var priority_content = "";
	 											if(queue.is_priority == "yes"){
	 												priority_content = '<small class="label label-rounded border-danger text-danger-600">PRIORITY</small>';
	 											}

	 											if(tbody.find('tr[data-queue_id="'+queue.id+'"]').length == 0){
	 												tbody.append($('<tr data-queue_id= "'+queue.id+'" data-source="'+queue.source+'"> <td>'+(eval(queue_table.find("tbody tr").length)+1)+'</td>'+'<td><div class="media-heading">'+ '<strong style="font-size: 18px;">'+queue.queue_no+'</strong> <span><i>- '+queue.name+'</i></span>'+ '</div>'+ '<span>'+queue.for_display+'</span> '+ priority_content+ '</div>'+'  </td> <td class="text-center">'+queue.created_at+'</td> <td class="text-center"><button class="label label-success btn-action" data-action="queue"><i class="icon-watch"></i> QUEUE</button> <button class="label label-success bg-danger btn-action" data-action="start"><i class="icon-play3"></i> Start</button></td><button class="label label-warning btn-action" data-action="cancel"><i class="icon-blocked"></i> Cancel</button> </tr>').fadeIn(500))
	 											}
	 											
											});
										}
									},
									error : function(jqXHR,textStatus,thrownError){
											console.log(jqXHR)
									}
							});
						});

				}

			setInterval(check_process, 4000);
				function check_process(){
					var queue_table = $("#queue_table");
					var tbody = queue_table.children("tbody");
					var type = queue_table.attr("data-service_type")
					var ids = "";

					$.each(tbody.find("tr"),function(index,row){
							// console.log();
							ids+=$(this).attr("data-queue_id")+",";
					}).promise().done(function(){
						 $.ajax("{{URL::to('api/queue/on-queue.json')}}", {
								type: 'POST',
								data : { api_token : _master_token,_type : type,_ids : ids},
								dataType: "json",
								async: true,
								success: function(data) {
									var result = data.data;

									if(result.length > 0){
										$.each(result,function(index,queue){
											var selector = tbody.find("tr[data-queue_id="+queue.id+"]");
											var _column = selector.children("td:last-child");
											
											switch(queue.status){
												case 'on_going':
													if(_column.children("button[data-action=stop]").length != 1){
														_column.find("button").fadeOut(200,function(){
																$(this).remove();

														});
														_column.append('<button class="label label-danger btn-action" data-action="stop"><i class="icon-stop"></i> Stop</button>');
													}

													if(["social_service","scholarship_program","scholarship"].indexOf(queue.service_type) != -1){

														if(_column.children("button[data-action=transfer]").length != 1){
															if(queue.is_transfer == "no" &&  selector.attr("data-source") != "mobile"){
																_column.append('<button class="label label-success btn-action" data-action="transfer"><i class="icon-share2"></i> Transfer</button>');
															}
														}else{
															if(queue.is_transfer == "yes"){
																_column.children("button[data-action=transfer]").fadeOut(200,function(){
																	$(this).remove()
																});
															}
														}
													}
												break;
												case 'cancelled':
												case 'completed':
													selector.fadeOut(300,function(){
														$(this).remove();
													});
												break;
											}
										});

										
									}
								},
								error : function(jqXHR,textStatus,thrownError){
										console.log(jqXHR)
								}
						});
					});
				}	
	});
</script>
@stop
