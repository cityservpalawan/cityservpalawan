@extends("backoffice._template.tv")

@section('page-styles')
<style type="text/css">
	.panel{
		 background: transparent!important; border: none; box-shadow: none;
	}
	.panel .text-semibold{
		 display: none;
	}

	.wrapper{ position: relative; }

	.panel .well{
		  border: none; color: #000; background: transparent; font-size: 55px; font-weight: bolder;
	}
	.jGrowl-notification{ width: 1000px; margin-top: 400px; margin-left: 30px;}
	.jGrowl-header strong{ font-size: 48px; text-align: center !important; }
	.jGrowl-message{ font-size: 40px; text-align: center !important; }

	.jGrowl-notification{ background-color: #0a6a12!important; }

	/*0b6909*/
</style>
@stop

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-6" style="position: absolute; top: 200px; left: 10px; ">
			<video id="video_player" style="width: 110%!important; height: 520px;" controls="true" autoplay="true" autobuffer> 
			  <source src="{{asset('backoffice/videos/1.mp4')}}" type="video/mp4" style="width: 100%; height: 300px;" volume="0.05">
				Your browser does not support the video tag.
			</video>
		</div>
	</div>
	<div id="queue_tv" style="position: absolute; bottom: 0px; width: 95%">
		<div class="col-md-8" >
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-4">
							<div class="panel panel-body border-top-success text-center service-container priority_lane ">
								<h6 class="no-margin text-semibold">PRIORITY LANE</h6>
								<h1 class="well bg-success-400" style="position: absolute; top: 5px; left: 10px; width: 360px;">
								@if($priority_lane)
								{{$priority_lane->queue_no}}
								@else
								- - -
								@endif	
								</h1>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-body border-top-success text-center service-container health_service" >
								<h6 class="no-margin text-semibold">HEALTH SERVICE</h6>
								<h1 class="well bg-success-400" style="position: absolute; top: 5px; left: 40px; width: 360px;">
								@if($health_service)
								{{$health_service->queue_no}}
								@else
								- - -
								@endif	
								</h1>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-body border-top-success text-center service-container scholarship_program" >
								<h6 class="no-margin text-semibold">SCHOLARSHIP PROG.</h6>
								<h1 class="well bg-success-400" style="position: absolute; top: 5px; left: 75px; width: 360px;">
								@if($scholarship_program)
								{{$scholarship_program->queue_no}}
								@else
								- - -
								@endif	
								</h1>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-body border-top-success text-center service-container legal_service" >
								<h6 class="no-margin text-semibold">LEGAL SERVICE</h6>
								<h1 class="well bg-success-400" style="position: absolute; top: 120px; left: 10px; width: 360px;">
								@if($legal_service)
								{{$legal_service->queue_no}}
								@else
								- - -
								@endif	
								</h1>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-body border-top-success text-center service-container social_service" >
								<h6 class="no-margin text-semibold">SOCIAL SERVICE</h6>
								<h1 class="well bg-success-400" style="position: absolute; top: 120px; left: 40px; width: 360px;">
								@if($social_service)
								{{$social_service->queue_no}}
								@else
								- - -
								@endif	
								</h1>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-body border-top-success text-center service-container administrative"  >
								<h6 class="no-margin text-semibold">ADMINISTRATIVE</h6>
								<h1 class="well bg-success-400"  style="position: absolute; top: 120px; left: 75px; width: 360px;">
								@if($administrative)
								{{$administrative->queue_no}}
								@else
								- - -
								@endif	
								</h1>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-4" style="visibility: hidden;">
							<div class="panel panel-body border-top-success text-center service-container administrative"  >
								<h6 class="no-margin text-semibold">ADMINISTRATIVE</h6>
								<h1 class="well bg-success-400"  >
								@if($administrative)
								{{$administrative->queue_no}}
								@else
								- - -
								@endif	
								</h1>
							</div>
							<div class="clearfix"></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section("page-scripts")
<script type="text/javascript" src="{{asset('backoffice/responsivevoice.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/notifications/jgrowl.min.js')}}"></script>
<script type="text/javascript">
	// var voicelist = responsiveVoice.getVoices();
	var _master_token = "{{env("APP_KEY")}}";
	$(function(){
		$("video").each(function(){ this.volume = 0.15; });

		var playlist = ["{{asset('backoffice/videos/1.mp4')}}","{{asset('backoffice/videos/2.mp4')}}","{{asset('backoffice/videos/3.mp4')}}","{{asset('backoffice/videos/4.mp4')}}","{{asset('backoffice/videos/5.mp4')}}","{{asset('backoffice/videos/6.mp4')}}","{{asset('backoffice/videos/7.mp4')}}","{{asset('backoffice/videos/8.mp4')}}","{{asset('backoffice/videos/9.mp4')}}","{{asset('backoffice/videos/10.mp4')}}","{{asset('backoffice/videos/11.mp4')}}","{{asset('backoffice/videos/12.mp4')}}"];
		var current_index = 0;
		var now_playing = document.getElementById('video_player');
		now_playing.addEventListener('ended', function(){
		    source = this.getElementsByTagName("source");
			this.pause();
		    if(playlist.length > 1){
		    	if(current_index == playlist.length-1){
		    		current_index = 0;
		    		// this.src = playlist[current_index];

		    		source[0].src = playlist[current_index]; 
		    	}else{
		    		current_index++;
		    		source[0].src = playlist[current_index];
		    	}
		    }
		    this.load();
		    this.play();
		}, false);


		// now_playing.onended = function(){
		// 		// ++curVideo;
		//     // if(current_index < nextVideo.length){    		
		//     //     videoPlayer.src = nextVideo[curVideo];        
		//     // } 

		//     if(playlist.length > 1){
		//     	if(current_index == playlist.length-1){
		//     		current_index = 0;
		//     		now_playing.src = playlist[current_index];
		//     	}else{
		//     		now_playing.src = playlist[++current_index];
		//     	}
		//     }
		//     		console.log(current_index)

		// }

	});
	(function get_queue() {
		$.ajax({
			url: '{{URL::to("api/queue/for-display.json")}}',
			data 	 :{api_token : _master_token},
			success: function(result) {
				var for_display = result.data;

				$.each(for_display,function(index,queue){
						// console.log(queue);
						$.ajax({
							url :"{{URL::to('api/queue/flash-for-display.json')}}",
							type : "POST",
							async : true,
							data : {api_token : _master_token,for_display_id : queue.id},
							success : function (result){
								var container = $(".service-container."+result.data.service_type)
								var queue_display = container.children("h1");
								$(this).addClass("text-danger");
								
								queue_display.fadeOut(200,function(){
									// $(this).text(result.data.queue_no).fadeIn(1500,function(){
									// 	$(this).css({ 'color' : "#f6ff8c"}).animate({ 'color' : "#000"},2000);
									// });

									$(this).text(result.data.queue_no).css({ 'color' : "#ef6c00", 'font-size' : "60px" }).fadeIn(500,function(){
										// queue_display.css({'color': "#000"})
										
									})

									setTimeout(function(){
										queue_display.css({'color': "#000", 'font-size' : "55px"})
										
									},4000);

									// queue_display.animate({
									// 	fontSize: "55px",
									// },2500);
									// $( "#clickme" ).click(function() {
									//   $( "#book" ).animate({
									//     opacity: 0.25,
									//     left: "+=50",
									//     height: "toggle"
									//   }, 5000, function() {
									//     // Animation complete.
									//   });
									// });
								});
								// responsiveVoice.speak("Now Serving Administrative Division S,0,0,1","US English Male", {pitch: 1,volume : 1});
								// responsiveVoice.speak(result.txt_speech,"US English Male", {pitch: 1,volume : 1});


								if ('speechSynthesis' in window) {

								    var text = result.txt_speech;
								    var msg = new SpeechSynthesisUtterance();
								    var voices = window.speechSynthesis.getVoices();
								    msg.voice = voices[0];
								    msg.rate = 10 / 10;
								    msg.pitch = 1;
								    msg.text = text;

								    msg.onend = function(e) {
								      console.log('Finished in ' + event.elapsedTime + ' seconds.');
								    };

								    speechSynthesis.speak(msg);
								}


								$.jGrowl(result.msg, {
										position: 'top-left',
										theme: 'bg-success-400',
										header: '<center style="font-size: 48px; font-weight: bold;">'+result.queue_no+'</center>',
										timer : 7000
								});
							}
						});
				});
				setTimeout(get_queue, 2000);

			},
			error: function() {
				setTimeout(get_queue, 2000);
			}
		});
	})();

</script>
@stop
