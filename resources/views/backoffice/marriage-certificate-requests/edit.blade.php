@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-camera"></i> <span class="text-semibold">Marriage Certificate Requests</span> - Edit a marriage certificate request.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.marriage_certificate_requests.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
				{{-- <a href="{{route('backoffice.marriage_certificate_requests.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a> --}}
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">	
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.marriage_certificate_requests.index')}}"> Marriage Certificate Requests</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-4">
			<div class="thumbnail pb-20">
				<h3 class="text-light text-center">{{$request->code}}</h3>
				{{--
				@if($request->filename)
				<div class="thumb thumb-rounded">
					<img src="{{$request->directory . '/thumbnails/' . $request->filename}}" alt="">
					<div class="caption-overflow">
						<span>
							<a href="{{$request->directory . '/resized/' . $request->filename}}"" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
						</span>
					</div>
				</div>
				@else
				<div class="thumb">
					<img src="{{asset('backoffice/images/no-thumbnail.jpg')}}" alt="">
				</div>
				@endif
				--}}

				<div class="caption text-center">
					<h6 class="text-semibold no-margin">{{$request->author ? "{$request->author->fname} {$request->author->lname}" : "Anonymous"}}<small class="display-block">{{$request->type}}</small></h6>
				</div>
			</div>

			<div class="panel panel-flat">
				<form method="POST">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="panel-heading">
						<h6 class="panel-title">Report Status</h6>
					</div>
					<div class="panel-body">
						<div class="form-group text-center">
						{!!Form::select("status", $statuses, $request->status, ['id' => "status", 'class' => "select text-center", 'style' => "width:200px;" ])!!}
						</div>
						<div class="text-left">
							<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Updating ..." class="btn btn-primary btn-raised btn-xs btn-loading">Update</button>
						</div>
					</div>
				</form>
			</div>
			
		</div>
		<div class="col-lg-9 col-md-9 col-sm-8">

			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Report says ...</h6>
					<div class="heading-elements">
						<span class="heading-text"><i class="icon-paperplane position-left text-success"></i> Sent {{$request->time_passed($request->created_at)}}</span>
	            	</div>
				</div>

				<div class="panel-body">
					{!!$request->content!!}
				</div>
			</div>

			<div class="panel panel-flat timeline-content col-md-12">
				<div class="panel-heading">
					<h6 class="panel-title">Geo Location</h6>
					<div class="heading-elements">
						<span class="heading-text"><i>*Location may vary depending on the device used.</i></span>
	            	</div>
				</div>

				<div class="panel-body">
					<div class="map-container map-marker-simple" style="height: 300px;"></div>
				</div>
			</div>
		</div>
	</div>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- CKEditor -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>

<!-- Gmaps -->
<script src="{{asset('backoffice/js/plugins/locationpicker/locationpicker.jquery.js')}}"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>

<!-- Fancybox -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/media/fancybox.min.js')}}"></script>

<!-- Static GMap -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key={{env('GOOGLE_MAP_KEY')}}"></script>

<!-- Gallery -->
<script type="text/javascript" src="{{asset('backoffice/js/pages/gallery.js')}}"></script>


<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('.daterange-single').daterangepicker({ 
			singleDatePicker: true,
			timePicker: true,
			timePickerIncrement: 1,
			locale: {
				format: 'YYYY-MM-DD h:mm A'
			}
		});

		function initialize() {

			// Set coordinates
			var myLatlng = new google.maps.LatLng({{$request->geo_lat}},{{$request->geo_long}});

			// Options
			var mapOptions = {
				zoom: 16,
				center: myLatlng,
			};

			// Apply options
			var map = new google.maps.Map($('.map-marker-simple')[0], mapOptions);
			var contentString = 'Report was sent here!';

			// Add info window
			var infowindow = new google.maps.InfoWindow({
				content: contentString
			});

			// Add marker
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				icon: "{{asset('backoffice/images/megaphone.png')}}",
			});

			infowindow.open(map, marker);

		};

		google.maps.event.addDomListener(window, 'load', initialize);

	});
</script>
@stop