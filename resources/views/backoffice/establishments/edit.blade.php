@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-office"></i> <span class="text-semibold">Establishments</span> - Edit an establishment.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.establishments.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
				<a href="{{route('backoffice.establishments.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.establishments.index')}}"> Establishments</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Establishment Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for your establishment.</p>

				<div class="form-group {{$errors->first('directory_id') ? 'has-error' : NULL}}">
					<label for="directory_id" class="control-label col-lg-2 text-right">Directory <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("directory_id", $directories, old('directory_id',$establishment->directory_id), ['id' => "directory_id", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('directory_id'))
						<span class="help-block">{{$errors->first('directory_id')}}</span>
						@endif
					</div>
				</div>
				
				<div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
					<label for="name" class="control-label col-lg-2 text-right">Name <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="name" id="name" placeholder="ex. Lorem Ipsum" maxlength="100" value="{{old('name',$establishment->name)}}">
						@if($errors->first('name'))
						<span class="help-block">{{$errors->first('name')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
					<label for="address" class="control-label col-lg-2 text-right">Address</label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="address" id="address" placeholder="ex. Some Place" value="{{old('address',$establishment->address)}}">
						@if($errors->first('address'))
						<span class="help-block">{{$errors->first('address')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('contact') ? 'has-error' : NULL}}">
					<label for="contact" class="control-label col-lg-2 text-right">Contact</label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="contact" id="contact" placeholder="ex. (00) 123-4567" value="{{old('contact',$establishment->contact)}}">
						@if($errors->first('contact'))
						<span class="help-block">{{$errors->first('contact')}}</span>
						@endif
					</div>
				</div>


				<div class="form-group {{$errors->first('status') ? 'has-error' : NULL}}">
					<label for="status" class="control-label col-lg-2 text-right">Status <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("status", $statuses, old('status',$establishment->status), ['id' => "status", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('status'))
						<span class="help-block">{{$errors->first('status')}}</span>
						@endif
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Geo Location</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
                <div class="col-lg-12 pr-20 pl-20">
                    <div id="post-property-map-container" class="fa fa-search form-control">
                        <input type="text" name="search" id="post-property-map" placeholder="Find your property here" style="width: 90%; border: 0;">
                    </div>
                    <div id="select-property-location" style="height: 350px;"></div>
                    {!!Form::hidden('geo_lat','',array('id' => "map_lat",'value'=> old('geo_lat',$geo_lat)))!!}
                    {!!Form::hidden('geo_long','',array('id' => "map_long",'value'=> old('geo_long',$geo_long)))!!}
                    <span class="help-block">You may search in the address bar or drag the marker to your desired location</span>
                </div>
	                   
			</div>
		</div>
		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.establishments.index')}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Gmaps -->
<script src="{{asset('backoffice/js/plugins/locationpicker/locationpicker.jquery.js')}}"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>

<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $("#select-property-location").locationpicker({
            location: {latitude: "{{old('geo_lat',$geo_lat)}}", longitude: "{{old('geo_long',$geo_long)}}"},
            zoom : 14,
            maxZoom : 18,
            minZoom : 14,
            radius: 20,
            inputBinding: {
                locationNameInput: $('#post-property-map')        
            },
            enableAutocomplete: true,
            onchanged: function(currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
                var loc =$(this).locationpicker('map').location;
                $("#map_lat").val(currentLocation.latitude);
                $("#map_long").val(currentLocation.longitude);
                $("#exact_address").val(loc.formattedAddress);
            }   
        });  

	});
</script>
@stop