<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{env('APP_TITLE',"Localhost")}} - Cpanel</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->
	
	<style type="text/css">
		.navbar-header {
		    min-width: 192px;
		}
		.navbar-brand>img {
		    margin-top: 2px;
		    height: 20px;
		}
		.bg-pepero {
			background-color: rgb(10, 127, 49);
		    border-color: rgb(10, 127, 49);
		    color: #fff;
		}
	</style>
	@yield('page-styles')

	<!-- Theme JS files -->
	@yield('page-scripts')
	<script type="text/javascript" src="{{asset('backoffice/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/ripple.min.js')}}"></script>
	<script type="text/javascript">
		$(function(){
			$("[rel=tooltip]").tooltip();
		});
	</script>
	<!-- /theme JS files -->
	{{--
	<!-- Session Timeout -->
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/extensions/session_timeout.min.js')}}"></script>
	<script type="text/javascript">
		$(function(){
			 // Session timeout
		    $.sessionTimeout({
		        heading: 'h5',
		        title: 'Session Timeout',
		        message: 'Your session is about to expire. Do you want to stay connected?',
		        ignoreUserActivity: true,
		        warnAfter: 300000,
		        redirAfter: 500000,
		        keepAliveUrl: '/',
		        redirUrl: '{{route('backoffice.lock')}}',
		        logoutUrl: '{{route('backoffice.logout')}}'
		    });
		});
	</script>
	--}}
</head>

<body>

	@include('backoffice._includes.header')


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-default">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user-material">
						<div class="category-content">
							<div class="sidebar-user-material-content">
								<a href="#">
								@if($auth->filename)
								<img src="{{asset($auth->directory . '/' . $auth->filename)}}" class="img-circle img-responsive" alt="">
								@else
								<img src="{{asset('backoffice/images/demo/users/Ph_seal_palawan.png')}}" class="img-circle img-responsive" alt="">
								@endif</a>
								<h6>{{$auth->fname}}</h6>
								<span class="text-size-small">{{Helper::account_type($auth->type)}}</span>
							</div>
														
							<div class="sidebar-user-material-menu">
								<a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
							</div>
						</div>
						
						<div class="navigation-wrapper collapse" id="user-nav">
							<ul class="navigation">
								<!-- <li><a href="#"><i class="icon-user-plus"></i> <span>My profile</span></a></li> -->
								<!-- <li><a href="#"><i class="icon-cog5"></i> <span>Account settings</span></a></li> -->
								<li><a href="{{route('backoffice.lock')}}"><i class="icon-lock5"></i> <span>Lock</span>
								<li><a href="{{route('backoffice.logout')}}"><i class="icon-switch2"></i> <span>Logout</span></a></li>
							</ul>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								@include('backoffice._includes.nav')
								
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				@yield('content')
				
				@yield('modals')
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
