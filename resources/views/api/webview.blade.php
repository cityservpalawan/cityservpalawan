<!DOCTYPE html>
<html>
<head>
	<title>{{isset($title) ? $title : env('APP_TITLE')}}</title>
</head>
<body>
	{!!$content!!}
</body>
</html>